<div class="form-group">
    <label for="input-{{ $name }}">{{$label}}</label>
    <input type="text" class="form-control {{ $clazz ?? '' }} {{ $errors->has($name) ? 'has-error' : '' }}"
        value="{{ old($name, $value ?? '') }}" id="input-{{ $name }}" name="{{ $name }}" {{$attribute ?? ''}}>
    @if ($errors->has($name))
        <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>
