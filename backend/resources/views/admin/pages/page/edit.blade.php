@extends('admin.templates.default', ['activePage' => 'page', 'titlePage' => __('Páginas')])

@section('title', 'Atualizar Página')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-1 text-dark">Atualizar Página</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-12">
                        <div class="card card-info card-outline">
                            <form action="{{ route('admin.page.update', ['resourceId' => $resource]) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <x-input name="title" value="{{ $resource->title }}" label="Título"
                                                attribute="required" />
                                        </div>
                                        <div class="col-sm-6">
                                            <x-input name="slug" value="{{ $resource->slug }}" label="Slug"
                                                attribute="required" clazz="input-slug" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <x-input name="meta_title" value="{{ $resource->meta_title }}"
                                                label="Título para SEO" attribute="required" />
                                        </div>
                                        <div class="col-sm-6">
                                            <x-input name="meta_description" value="{{ $resource->meta_description }}"
                                                label="Descrição para SEO" attribute="required" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="textarea-content">Conteúdo</label>
                                                <textarea type="text"
                                                    class="form-control textarea {{ $errors->has('content') ? 'has-error' : '' }}"
                                                    id="textarea-content" name="content"
                                                    required>{{ old('content', $resource->content) }}</textarea>
                                                @if ($errors->has('content'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('content') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary float-right">Atualizar</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.card -->
                    </div>

                    <!-- ./col -->
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
