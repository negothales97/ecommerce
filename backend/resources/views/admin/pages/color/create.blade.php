@extends('admin.templates.default', ['activePage' => 'color', 'titlePage' => __('Cores')])

@section('title', 'Adicionar Cor')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-1 text-dark">Adicionar Cor</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-6">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Dados</h3>
                            </div>
                            <form action="{{ route('admin.color.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="name">Nome</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                    name="name" id="name" value="value="{{old('name')}}"">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file"
                                                            class="form-control custom-file-input {{ $errors->has('file') ? 'is-invalid' : '' }}"
                                                            name="file" id="file">
                                                        <label class="custom-file-label" for="file">Escolha um arquivo</label>
                                                    </div>
                                                </div>
                                                @if ($errors->has('file'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('file') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary float-right">Adicionar</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.card -->
                    </div>

                    <!-- ./col -->
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
