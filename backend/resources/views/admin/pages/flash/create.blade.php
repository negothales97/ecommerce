@extends('admin.templates.default', ['activePage' => 'flash', 'titlePage' => __('Promoções Relâmpago')])

@section('title', 'Adicionar Promoção Relâmpago')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-1 text-dark">Adicionar Promoção Relâmpago</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-12">
                        <div class="card card-info card-outline">
                            <form action="{{ route('admin.promotion.flash.store') }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="name">Nome</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                    name="name" id="name" value="{{old('name')}}">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="discount">Desconto</label>
                                                <div class="input-group">
                                                    <input type="text"
                                                        class="form-control input-money {{ $errors->has('discount') ? 'is-invalid' : '' }}"
                                                        name="discount" id="discount" value="{{old('discount')}}">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">%</span>
                                                    </div>
                                                </div>
                                                @if ($errors->has('discount'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('discount') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="limit_date">Data limite</label>
                                                <input type="text"
                                                    class="form-control input-date {{ $errors->has('limit_date') ? 'is-invalid' : '' }}"
                                                    name="limit_date" id="limit_date" value="{{old('limit_date')}}">
                                                @if ($errors->has('limit_date'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('limit_date') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="limit_hour">Hora limite</label>
                                                <input type="text"
                                                    class="form-control input-hour {{ $errors->has('limit_hour') ? 'is-invalid' : '' }}"
                                                    name="limit_hour" id="limit_hour" value="{{old('limit_hour')}}">
                                                @if ($errors->has('limit_hour'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('limit_hour') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary float-right">Adicionar</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.card -->
                    </div>

                    <!-- ./col -->
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
