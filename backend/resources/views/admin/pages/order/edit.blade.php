@extends('admin.templates.default', ['activePage' => 'order', 'titlePage' => __('Pedidos')])

@section('title', 'Atualizar Pedido')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-1 text-dark">Atualizar Pedido</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <form action="{{ route('admin.order.update', ['order' => $order->id]) }}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Dados Principais</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="tracking_code">Código de rastreio</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('tracking_code') ? 'is-invalid' : '' }}"
                                                    name="tracking_code" id="tracking_code" value="{{ old('tracking_code', $order->tracking_code) }}">
                                                @if ($errors->has('tracking_code'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('tracking_code') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="payment_status">Status de Pagamento</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('payment_status') ? 'is-invalid' : '' }}"
                                                    name="payment_status" id="payment_status" value="{{ old('payment_status', $order->payment_status) }}"
                                                    readonly>
                                                @if ($errors->has('payment_status'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('payment_status') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="status">Status do pedido</label>
                                                <select name="status" id="status" class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}">
                                                    <option selected disabled>Selecione..</option>
                                                    <option value="0" {{$order->status == 'Pedido gerado' ? 'selected' : ''}}>Pedido gerado</option>
                                                    <option value="1" {{$order->status == 'Nota Fiscal emitida' ? 'selected' : ''}}>Nota Fiscal emitida</option>
                                                    <option value="2" {{$order->status == 'Pedido enviado' ? 'selected' : ''}}>Pedido enviado</option>
                                                </select>
                                                @if ($errors->has('status'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('status') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="subtotal">Subtotal</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">R$</span>
                                                    </div>
                                                    <input type="text"
                                                        value="{{ convertMoneyUsaToBrazil($order->subtotal) }}"
                                                        class="form-control input-money {{ $errors->has('subtotal') ? 'is-invalid' : '' }}"
                                                        name="subtotal" id="subtotal" readonly>
                                                </div>
                                                @if ($errors->has('subtotal'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('subtotal') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="shipping_price">Frete</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">R$</span>
                                                    </div>
                                                    <input type="text"
                                                        value="{{ convertMoneyUsaToBrazil($order->shipping_price) }}"
                                                        class="form-control input-money {{ $errors->has('shipping_price') ? 'is-invalid' : '' }}"
                                                        name="shipping_price" id="shipping_price" readonly>
                                                </div>
                                                @if ($errors->has('shipping_price'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('shipping_price') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="total">Total</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">R$</span>
                                                    </div>
                                                    <input type="text" value="{{ convertMoneyUsaToBrazil($order->total) }}"
                                                        class="form-control input-money {{ $errors->has('total') ? 'is-invalid' : '' }}"
                                                        name="total" id="total" readonly>
                                                </div>
                                                @if ($errors->has('total'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('total') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="name">Nome</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                    name="name" id="name" value="{{ old('name', $order->customer->name) }}"
                                                    readonly>
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">E-mail</label>
                                                <input type="email"
                                                    class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                                    name="email" id="email"
                                                    value="{{ old('email', $order->customer->email) }}" readonly>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="birthday">Data de nascimento</label>
                                                <input type="text"
                                                    class="form-control input-date {{ $errors->has('birthday') ? 'is-invalid' : '' }}"
                                                    name="birthday" id="birthday" readonly
                                                    value="{{ old('birthday', convertDateUsaToBrazil($order->customer->birthday)) }}">
                                                @if ($errors->has('birthday'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('birthday') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="cellphone">Telefone</label>
                                                <input type="text"
                                                    class="form-control input-phone {{ $errors->has('cellphone') ? 'is-invalid' : '' }}"
                                                    name="cellphone" id="cellphone"
                                                    value="{{ old('cellphone', $order->customer->cellphone) }}" readonly>
                                                @if ($errors->has('cellphone'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('cellphone') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="document_type">Tipo de Documento</label>

                                                <select name="document_type"
                                                    class="form-control {{ $errors->has('document_type') ? 'is-invalid' : '' }}"
                                                    id="document_type">
                                                    <option disabled selected>Selecione..</option>
                                                    <option value="cpf"
                                                        {{ $order->customer->document_type == 'cpf' ? 'selected' : '' }}>CPF
                                                    </option>
                                                    <option value="cnpj"
                                                        {{ $order->customer->document_type == 'cnpj' ? 'selected' : '' }}>
                                                        CNPJ
                                                    </option>

                                                </select>
                                                @if ($errors->has('document_type'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('document_type') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Documento</label>
                                                <input type="text"
                                                    class="form-control input-cpf {{ $order->customer->document_type == 'cnpj' ? 'display-none' : '' }} {{ $errors->has('document_number') ? 'is-invalid' : '' }}"
                                                    name="document_number" id="cpf"
                                                    {{ $order->customer->document_type == 'cnpj' ? 'disabled' : '' }}
                                                    value="{{ old('document_number', $order->customer->document_number) }}">
                                                <input type="text"
                                                    class="form-control input-cnpj {{ $order->customer->document_type == 'cpf' || $order->customer->document_type == null ? 'display-none' : '' }} {{ $errors->has('document_number') ? 'is-invalid' : '' }}"
                                                    name="document_number" id="cnpj"
                                                    {{ $order->customer->document_type == 'cpf' || $order->customer->document_type == null ? 'disabled' : '' }}
                                                    value="{{ old('document_number', $order->customer->document_number) }}">
                                                @if ($errors->has('document_number'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('document_number') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>

                        <!-- ./col -->
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Endereço de entrega</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="zip_code">Cep</label>
                                                <input type="text"
                                                    value="{{ $order->customer->addresses()->first()->zip_code }}"
                                                    class="form-control input-cep {{ $errors->has('zip_code') ? 'is-invalid' : '' }}"
                                                    name="zip_code" id="zip_code">
                                                @if ($errors->has('zip_code'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('zip_code') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="street">Rua</label>
                                                <input type="text"
                                                    value="{{ $order->customer->addresses()->first()->street }}"
                                                    class="form-control  {{ $errors->has('street') ? 'is-invalid' : '' }}"
                                                    name="street" id="street">
                                                @if ($errors->has('street'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('street') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="number">Número</label>
                                                <input type="text"
                                                    value="{{ $order->customer->addresses()->first()->number }}"
                                                    class="form-control {{ $errors->has('number') ? 'is-invalid' : '' }}"
                                                    name="number" id="number">
                                                @if ($errors->has('number'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('number') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="district">Bairro</label>
                                                <input type="text"
                                                    value="{{ $order->customer->addresses()->first()->district }}"
                                                    class="form-control {{ $errors->has('district') ? 'is-invalid' : '' }}"
                                                    name="district" id="district">
                                                @if ($errors->has('district'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('district') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="city">Cidade</label>
                                                <input type="text"
                                                    value="{{ $order->customer->addresses()->first()->city }}"
                                                    class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}"
                                                    name="city" id="city">
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('city') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="state">Estado</label>
                                                <input type="text"
                                                    value="{{ $order->customer->addresses()->first()->state->sigla }}"
                                                    class="form-control {{ $errors->has('state') ? 'is-invalid' : '' }}"
                                                    name="state" id="state">
                                                @if ($errors->has('state'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('state') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>

                        <!-- ./col -->
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Produtos</h3>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Qtd</th>
                                                <th>Preço Unitário</th>
                                                <th>Subtotal</th>
                                                {{-- <th>Ações</th>
                                                --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($order->products as $product)
                                                <tr>
                                                    <td>{{ $product->product->id }}</td>
                                                    <td>{{ $product->product->name }}</td>
                                                    <td>{{ $product->quantity }}</td>
                                                    <td>R$ {{ convertMoneyUsaToBrazil($product->unit_price) }}</td>
                                                    <td>R$
                                                        {{ convertMoneyUsaToBrazil($product->unit_price * $product->quantity) }}
                                                    </td>
                                                    {{-- <td
                                                        class="text-center align-middle py-0">
                                                        <div class="btn-group btn-group-sm">
                                                            <a href="{{ route('admin.order.edit', ['order' => $order->id]) }}"
                                                                class="btn btn-info">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <button
                                                                href="{{ route('admin.order.delete', ['order' => $order->id]) }}"
                                                                class="btn btn-danger btn-delete">
                                                                <i class="fa fa-trash"></i>
                                                            </button>

                                                        </div>
                                                    </td> --}}
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan=6>Nenhum dado cadastrado</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-footer">
                                </div>

                            </div>

                            <!-- /.card -->
                        </div>

                        <!-- ./col -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button class="btn btn-primary float-right">Atualizar</button>
                        </div>
                    </div>
                </form>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#document_type').on('change', function() {
        let documentType = $(this).val();
        if (documentType == "cpf") {
            $("#cnpj").addClass('display-none');
            $("#cpf").removeClass('display-none')

            $("#cnpj").attr('disabled', true);
            $("#cpf").attr('disabled', false);
        } else {
            $("#cpf").addClass('display-none');
            $("#cnpj").removeClass('display-none');

            $("#cnpj").attr('disabled', false);
            $("#cpf").attr('disabled', true)
        }
    });

</script>
@endsection
