@extends('admin.templates.default', ['activePage' => 'banner', 'titlePage' => __('Banners')])

@section('title', 'Banners')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Banners</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <div class="float-right">
                            <a href="{{ route('admin.design.banner.create') }}">
                                <button class="btn btn-success act-include">
                                    Adicionar
                                </button>
                            </a>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-12">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Lista de Banners</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-3">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Imagem</th>
                                            <th>Nome</th>
                                            <th>Status</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($resources as $resource)
                                            <tr>
                                                <td class="text-center align-middle py-0">
                                                    <img class="color-img"
                                                        src="{{ asset('uploads/design/banner/thumbnail') }}/{{ $resource->file }}"
                                                        alt="{{ $resource->name }}">
                                                </td>
                                                <td>{{ $resource->name }}</td>
                                                <td>{{ $resource->status }}</td>
                                                <td class="text-center align-middle py-0">
                                                    <div class="btn-group btn-group-sm">
                                                        <a href="{{ route('admin.design.banner.change', ['resourceId' => $resource]) }}"
                                                            class="btn btn-info btnChangeSubproduct btn-sm"
                                                            title="Utilizar Subproduto">
                                                            <i
                                                                class="fas fa-toggle-{{ $resource->status == "Ativo" ? 'on' : 'off' }}"></i>
                                                        </a>
                                                        <a href="{{ route('admin.design.banner.edit', ['resourceId' => $resource->id]) }}"
                                                            class="btn btn-info">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <button
                                                            href="{{ route('admin.design.banner.delete', ['resourceId' => $resource->id]) }}"
                                                            class="btn btn-danger btn-delete">
                                                            <i class="fa fa-trash"></i>
                                                        </button>

                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan=6>Nenhum dado cadastrado</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>

                    <!-- ./col -->
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
