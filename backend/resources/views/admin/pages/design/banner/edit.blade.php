@extends('admin.templates.default', ['activePage' => 'banner', 'titlePage' => __('Banners')])

@section('title', 'Atualizar Banner')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-1 text-dark">Atualizar Banner</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-6">
                        <div class="card card-info card-outline">
                            <form action="{{ route('admin.design.banner.update', ['resourceId' => $resource]) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="name">Nome</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                    name="name" id="name" value="{{old('name', $resource->name)}}">
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="link">Link</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('link') ? 'is-invalid' : '' }}"
                                                    name="link" id="link" value="{{old('link', $resource->link)}}">
                                                @if ($errors->has('link'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('link') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="target">Abertura</label>
                                                <select name="target" id="target" class="form-control">
                                                    <option disabled selected>Selecione..</option value="">
                                                    <option value="0" {{old('target',$resource->target) == '0' ? 'selected' : ''}}>Mesma Aba</option value="">
                                                    <option value="1" {{old('target',$resource->target) == '1' ? 'selected' : ''}}>Nova Aba</option value="">
                                                </select>
                                                
                                                @if ($errors->has('target'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('target') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="file">Imagem</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file"
                                                            class="form-control custom-file-input {{ $errors->has('file') ? 'is-invalid' : '' }}"
                                                            name="file" id="file">
                                                        <label class="custom-file-label" for="file">Escolha um
                                                            arquivo</label>
                                                    </div>
                                                </div>
                                                @if ($errors->has('file'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('file') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-primary float-right">Atualizar</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-6">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Imagem</h3>
                            </div>
                            <div class="card-body">

                                <img  class="img-preview-color" src="{{ asset('uploads/design/banner/thumbnail') }}/{{ $resource->file }}"
                                    alt="{{ $resource->name }}">
                            </div>

                        </div>
                        <!-- /.card -->
                    </div>

                    <!-- ./col -->
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection