@extends('admin.templates.default', ['activePage' => 'configuration', 'titlePage' => __('Configurações')])

@section('title', 'Atualizar Configurações')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-1 text-dark">Atualizar Configurações</h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <form action="{{ route('admin.configuration.update') }}" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Dados</h3>
                                </div>

                                @csrf
                                @method('put')
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="cellphone">Celular</label>
                                                <input type="text"
                                                    class="form-control input-phone {{ $errors->has('cellphone') ? 'is-invalid' : '' }}"
                                                    name="cellphone" id="cellphone"
                                                    value="{{ old('cellphone', $configuration->cellphone) }}">
                                                @if ($errors->has('cellphone'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('cellphone') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">E-mail</label>
                                                <input type="email"
                                                    class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                                    name="email" id="email"
                                                    value="{{ old('email', $configuration->email) }}">
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="opening_hours">Horário de atendimento</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('opening_hours') ? 'is-invalid' : '' }}"
                                                    name="opening_hours" id="opening_hours"
                                                    value="{{ old('opening_hours', $configuration->opening_hours) }}">
                                                @if ($errors->has('opening_hours'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('opening_hours') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="cnpj">CNPJ</label>
                                                <input type="text"
                                                    class="form-control input-cnpj {{ $errors->has('cnpj') ? 'is-invalid' : '' }}"
                                                    name="cnpj" id="cnpj"
                                                    value="{{ old('cnpj', $configuration->cnpj) }}">
                                                @if ($errors->has('cnpj'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('cnpj') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="about">Sobre a marca</label>
                                                <textarea name="about" id="input-about"
                                                    class="form-control textarea {{ $errors->has('about') ? 'is-invalid' : '' }}"
                                                    required rows="5">{{ old('about', $configuration->about) }}</textarea>
                                                @if ($errors->has('about'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('about') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="zip_code">CEP</label>
                                                <input type="text"
                                                    class="form-control input-cep {{ $errors->has('zip_code') ? 'is-invalid' : '' }}"
                                                    name="zip_code" id="zip_code"
                                                    value="{{ old('zip_code', $configuration->zip_code) }}">
                                                @if ($errors->has('zip_code'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('zip_code') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="street">Rua</label>
                                                <input type="texy"
                                                    class="form-control {{ $errors->has('street') ? 'is-invalid' : '' }}"
                                                    name="street" id="street"
                                                    value="{{ old('street', $configuration->street) }}">
                                                @if ($errors->has('street'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('street') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="number">Número</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('number') ? 'is-invalid' : '' }}"
                                                    name="number" id="number"
                                                    value="{{ old('number', $configuration->number) }}">
                                                @if ($errors->has('number'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('number') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="district">Bairro</label>
                                                <input type="text"
                                                    class="form-control {{ $errors->has('district') ? 'is-invalid' : '' }}"
                                                    name="district" id="district"
                                                    value="{{ old('district', $configuration->district) }}">
                                                @if ($errors->has('district'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('district') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="city">Cidade</label>
                                                <input type="texy"
                                                    class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}"
                                                    name="city" id="city" value="{{ old('city', $configuration->city) }}">
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('city') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="state_id">Estado</label>
                                                <select name="state_id" id="state_id"
                                                    class="form-control {{ $errors->has('state_id') ? 'is-invalid' : '' }}">
                                                    <option disabled selected>Selecione...</option>
                                                    @foreach ($states as $state)
                                                        <option value="{{ $state->id }}"
                                                            {{ $configuration->state_id == $state->id ? 'selected' : '' }}>
                                                            {{ $state->sigla }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('state_id'))
                                                    <span class="help-block">
                                                        <small>
                                                            <strong>{{ $errors->first('state_id') }}</strong>
                                                        </small>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- ./col -->
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-info card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">Redes Sociais</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <x-input name="instagram" value="{{ $configuration->instagram }}"
                                                label="Instagram" />
                                        </div>
                                        <div class="col-sm-6">
                                            <x-input name="facebook" value="{{ $configuration->facebook }}"
                                                label="Facebook" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <x-input name="youtube" value="{{ $configuration->youtube }}"
                                                label="Youtube" />
                                        </div>
                                        <div class="col-sm-6">
                                            <x-input name="linkedin" value="{{ $configuration->linkedin }}"
                                                label="Linkedin" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <x-input name="tiktok" value="{{ $configuration->tiktok }}"
                                                label="TikTok" />
                                        </div>
                                        <div class="col-sm-6">
                                            <x-input name="twitter" value="{{ $configuration->twitter }}"
                                                label="Twitter" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <x-input name="pinterest" value="{{ $configuration->pinterest }}"
                                                label="Pinterest" />
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="row">
                        <div class="col-sm-12 p-3">
                            <button class="btn btn-primary float-right" type="submit">Atualizar</button>
                        </div>
                    </div>
                    <!-- ./col -->
                </form>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
