@extends('admin.templates.default', ['activePage' => 'product', 'titlePage' => __('Produtos')])

@section('title', 'Produtos')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Produtos</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <div class="float-right">
                            <a href="{{ route('admin.product.create') }}">
                                <button class="btn btn-success act-include">
                                    Adicionar
                                </button>
                            </a>
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-12">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3 class="card-title">Lista de Produtos</h3>
                                <div class="card-tools">
                                    <?php
                                    $paginate = $products;

                                    $link_limit = 7;

                                    $filters = '';
                                    // $filters .= '&init_date=' . request('init_date');
                                    // $filters .= '&final_date=' . request('final_date');
                                    // $filters .= '&orderby=' . request('orderby');
                                    // $filters .= '&ordenation=' . request('ordenation');
                                    // $filters .= '&reference=' . request('reference');
                                    // $filters .= '&name=' . request('name');
                                    // $filters .= '&tag_id=' . request('tag_id');
                                    // $filters .= '&provider_id=' . request('provider_id');
                                    // $filters .= '&alert=' . request('alert');
                                    ?>

                                    @if ($paginate->lastPage() > 1)
                                        <ul class="pagination pagination-sm float-right">
                                            <li class="page-item {{ $paginate->currentPage() == 1 ? ' disabled' : '' }}">
                                                <a class="page-link" href="{{ $paginate->url(1) . $filters }}">«</a>
                                            </li>
                                            @for ($i = 1; $i <= $paginate->lastPage(); $i++)
                                                <?php
                                                $half_total_links = floor($link_limit / 2);
                                                $from = $paginate->currentPage() - $half_total_links;
                                                $to = $paginate->currentPage() + $half_total_links;
                                                if ($paginate->currentPage() < $half_total_links) { $to +=$half_total_links
                                                    - $paginate->currentPage();
                                                    }
                                                    if ($paginate->lastPage() - $paginate->currentPage() <
                                                        $half_total_links) { $from -=$half_total_links - ($paginate->
                                                        lastPage() - $paginate->currentPage()) - 1;
                                                        }
                                                        ?>
                                                        @if ($from < $i && $i < $to)
                                                            <li
                                                                class="page-item {{ $paginate->currentPage() == $i ? ' active' : '' }}">
                                                                <a class="page-link" href="{{ $paginate->url($i) . $filters }}">{{ $i }}</a>
                                                            </li>
                                                        @endif
                                            @endfor
                                            <li
                                                class="page-item {{ $paginate->currentPage() == $paginate->lastPage() ? ' disabled' : '' }}">
                                                <a class="page-link" href="{{ $paginate->url($paginate->lastPage()) . $filters }}">»</a>
                                            </li>
                                        </ul>
                                    @endif
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-3">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Ult. Atualização</th>
                                            <th>Disponível</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($products as $product)
                                            <tr>
                                            <td>
                                                <img class="photo" src="{{$product->main_image}}" alt="{{$product->name}}">
                                            </td>
                                                <td>{{ $product->name }}</td>
                                                <td>{{ $product->updated_at }}</td>
                                                <td class="text-center align-middle py-0">
                                                    <div class="btn-group btn-group-sm">
                                                        <a href="{{ route('admin.product.status', ['product' => $product->id]) }}"
                                                            class="btn btn-info">
                                                            <i class="fas fa-{{$product->toggle}}"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="text-center align-middle py-0">
                                                    <div class="btn-group btn-group-sm">
                                                        <a href="{{ route('product', ['slug' => $product->slug]) }}" target="_blank"
                                                            class="btn btn-info" title="Visualizar no Site">
                                                            <i class="fas fa-external-link-alt"></i>
                                                        </a>
                                                        <a href="{{ route('admin.product.edit', ['product' => $product->id]) }}"
                                                            class="btn btn-info">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <button onclick="deleteItem(this, 1)"
                                                            data-href="{{ route('admin.product.delete', ['product' => $product->id]) }}"
                                                            class="btn btn-danger act-delete">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan=6>Nenhum dado cadastrado</td>
                                            </tr>
                                        @endforelse

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>

                    <!-- ./col -->
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
