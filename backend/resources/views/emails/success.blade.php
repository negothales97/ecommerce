<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <style type="text/css">
        section.content {
            background: #efefef;
        }

        section.content h2 {
            font-size: 30px;
            font-weight: 900;
            margin-top: 60px;
            margin-bottom: 35px;
        }

        @media(max-width: 768px) {
            section.content h2 {
                font-size: 25px;
                font-weight: 900;
                margin-top: 25px;
                margin-bottom: 15px;
            }
        }

        .checkout h5 {
            margin-bottom: 15px;
        }

        .checkout ul {
            padding: 0;
            list-style: none;
        }

        .checkout li {
            margin: 5px 0;
        }

        .checkout img {
            width: 100%;
            max-width: 13px;
            margin-right: 10px;
            margin-top: -3px;
        }

        .checkout img#card-checkout {
            max-width: none;
            margin: 0 0 15px 0;
        }

        .box-register {
            background: #fff;
            text-align: center;
            padding: 30px 45px 60px;
            margin: 40px 0;
            box-shadow: 1px 1px 5px rgb(0 0 0 / 21%);
            border-radius: 5px;
        }

        .box-register p {
            font-weight: bold;
            margin-bottom: 35px;
        }

        .box-register label {
            font-size: 13px;
            font-weight: 500;
            margin-bottom: 3px;
            letter-spacing: 0.3px;
        }

        .box-register input {
            width: 100%;
            border-radius: 5px;
            padding: 7px 15px;
        }

        .box-register .custom-control.custom-radio.custom-control-inline {
            margin-bottom: 10px;
        }

        .box-register button {
            width: 100%;
            background: #f7941d;
            border: none;
            color: #fff;
            font-weight: bold;
            padding: 8px;
            border-radius: 5px;
        }


        .box-order-placed {
            font-family: 'Lato', sans-serif;
            padding: 50px 0 0;
        }

        .box-order-placed img {
            width: 100%;
            max-width: 50px;
        }

        .box-order-placed h4 {
            font-size: 20px;
            color: #48b02c;
            font-weight: 900;
            margin: 20px 0 5px;
        }

        .box-order-placed span {
            color: #000;
            font-weight: 900;
            font-size: 18px;
            display: table;
            margin: 0 auto 35px;
        }

        .box-order-placed .box-steps {
            float: left;
            width: 33%;
            position: relative;
            padding: 0 10px;
        }

        .box-order-placed .box-steps img {
            max-width: 30px;
        }

        .box-order-placed .box-steps small {
            font-size: 12px;
            display: flex;
            line-height: 13px;
            margin-top: 7px;
        }

        .box-order-placed .box-steps small#waiting {
            color: #b04f39;
        }

        .box-order-placed .box-steps img#step3 {
            max-width: 40px;
            margin-top: 0.2px;
        }

        .box-order-placed .box-steps img.next {
            position: absolute;
            top: 10px;
            right: -10px;
            max-width: 15px !important;
        }


        .box-order-placed #next-step {
            text-align: left;
            padding: 20px;
            margin-top: 130px;
        }

        .box-order-placed #next-step h5 {
            font-weight: 900;
        }

        .box-order-placed #next-step p {
            letter-spacing: 0;
        }

    </style>
</head>

<body>
    <section class="content">
        <section class="checkout">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3">
                        <div class="box-register box-order-placed">
                            <img src="{{ asset('img/tick.png') }}" alt="Check">
                            <h4>Pedido realizado</h4>
                            <span>#{{ session('order_id') }}</span>
                            <div class="box-steps">
                                <img src="{{ asset('img/step1.png') }}" alt="Icon">
                                <small id="waiting">Aguardando confirmação de pagamento</small>
                                <img class="next" src="{{ asset('img/next.png') }}" alt="Icon">
                            </div>
                            <div class="box-steps">
                                <img src="{{ asset('img/step2.png') }}" alt="Icon">
                                <small>Emitindo sua nota fiscal e embalando</small>
                                <img class="next" src="{{ asset('img/next.png') }}" alt="Icon">
                            </div>
                            <div class="box-steps">
                                <img id="step3" src="{{ asset('img/step3.png') }}" alt="Icon">
                                <small>Pedido enviado para o seu endereço</small>
                            </div>
                            <div id="next-step">
                                <h5>Próximos passos</h5>
                                <p>
                                    1. Estamos processando seu pagamento e assim que ele for confirmado te notificaremos
                                    por e-mail.<br><br>

                                    2. Com o pagamento aprovado, seu pedido sairá para entrega e você receberá um link
                                    para acompanhá-lo.<br><br>

                                    3. Assim que o pedido for entregue, você receberá um link para avaliar o produto e o
                                    atendimento recebido.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

</body>

</html>
