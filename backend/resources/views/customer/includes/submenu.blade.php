<section id="submenu-products-desktop">
    <div class="container">
        <div class="row">
            <div class="col">
                <p class="text-center">Escolha a empresa</p>
            </div>
        </div>
        <div id="submenu-categories">
            <div class="row">
            </div>
        </div>
        <div class="row mt-3 mb-4 loading" style="display:none">
            <div class="col">
                <img src="{{ asset('img/loading.gif') }}" width="100" alt="Carregando" class="img-loading">

            </div>
        </div>
    </div>
</section>
