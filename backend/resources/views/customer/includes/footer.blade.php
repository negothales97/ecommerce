<footer>
    <section class="info">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h4>Atendimento</h4>
                    <ul>
                        <li><a href="#"><img id="whatsapp" src="{{ asset('img/whatsapp.png') }}" alt="Whatsapp">(11)
                                98822-2222</a></li>
                        <li><a href="#"><img id="email" src="{{ asset('img/email.png') }}"
                                    alt="E-mail">sac@keepjeans.com.br</a></li>
                    </ul>
                    <ul>
                        <li>Horário de atendimento: <br>
                            Segunda à sexta-feira das 9h às 18hs</li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <h4>Informações</h4>
                    <ul>
                        <li><a href="#">Quem somos</a></li>
                        <li><a href="#">Sobre as entregas</a></li>
                        <li><a href="#">Formas de pagamento</a></li>
                        <li><a href="#">Política de privacidade</a></li>
                        <li><a href="#">Política de troca</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <h4>Lojas</h4>
                    <ul>
                        <li><a href="#">Conheça nossas lojas</a></li>
                        <li><a href="#">Trabalhe conosco</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="payment">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h4>Formas de Pagamento</h4>
                    <img id="img-payment" src="{{ asset('img/payment.png') }}" alt="Pagamento">
                </div>
                <div class="col-sm-6">
                    <h4>Segurança</h4>
                    <img src="{{ asset('img/security.png') }}" alt="Segurança">
                </div>
            </div>
        </div>
    </section>
    <div class="address">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>
                        Keep Jeens Confecções<br>
                        {{ $configuration->cnpj }}
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <strong>Endereço para compra no atacado</strong> <br>
                        {{ $configuration->street . ', ' . $configuration->number }} <br>
                        {{ $configuration->district . ', ' . $configuration->city . ',' . $configuration->sigla }}<br>
                        {{ $configuration->zip_code }}
                    </p>
                </div>
                <div class="col-md-4">
                    <a href="#">Endereços para compra no varejo</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <small>&copy; 2020 Keep Jeans. Todos os direitos reservados.</small>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
    integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<!-- Inputmask -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<!-- MaskMoney -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
{{-- Axios --}}
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
{{-- EZPlus Zoom --}}
<script type="text/javascript" src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.6/src/jquery.ez-plus.js">
</script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
{{-- Mascaras --}}
<script src="{{ asset('js/mask.js') }}"></script>
<script src="{{asset('js/menu-mobile.js')}}"></script>
<script type="text/javascript">
    var uri = "{{ route('api.product.index') }}";
    $('.slider-carousel').slick({
        prevArrow: $('.prev-carousel'),
        nextArrow: $('.next-carousel'),
        centerMode: true,
        slidesToShow: 4,
        slidesToScroll: 2,
        autoplay: false,
        autoplaySpeed: 2500,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });


    $('.slider-carousel-news').slick({
        prevArrow: $('.prev-carousel-news'),
        nextArrow: $('.next-carousel-news'),
        centerMode: true,
        slidesToShow: 4,
        slidesToScroll: 2,
        autoplay: false,
        autoplaySpeed: 2500,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    $('.slider-carousel-promotion').slick({
        prevArrow: $('.prev-carousel-promotion'),
        nextArrow: $('.next-carousel-promotion'),
        centerMode: true,
        slidesToShow: 4,
        slidesToScroll: 2,
        autoplay: false,
        autoplaySpeed: 2500,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    $(document).on('click', '.scroll', function(event) {
        var tela = $(window).width();
        if (tela < 768) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 120
            }, 800);
        } else {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 140
            }, 800);
        }

    });

    $('.btn-toggle').click(function() {
        $('#menu-mobile').show('slide', {
            direction: 'left'
        }, 1000);
        $('#overlay').css('display', 'block');
    });

    $('#close-menu').click(function() {
        $('#menu-mobile').hide('slide', {
            direction: 'left'
        }, 1000);
        $('#overlay').css('display', 'none');
    });

    $('#main-menu-mobile a').click(function() {
        $('#menu-mobile').hide('slide', {
            direction: 'left'
        }, 1000);
        $('#overlay').css('display', 'none');
    });

    

    $(document).ready(function() {
        $('#btn-filter-mobile').click(function() {
            $(this).siblings(".box-filter-mobile").toggle();
        });
    });

    $(document).ready(function() {
        $('#btn-freight').click(function() {
            $(this).siblings("#option-freight").toggle();
            $("i", this).toggleClass("fa-caret-down fa-caret-up");
        });
    });

    $(document).ready(function() {
        $('#btn-description').click(function() {
            $(this).siblings("#option-description").toggle();
            $("i", this).toggleClass("fa-caret-down fa-caret-up");
        });
    });

    $(document).ready(function() {
        $('#btn-size').click(function() {
            $(this).siblings("#option-size").toggle();
            $("i", this).toggleClass("fa-caret-down fa-caret-up");
        });
    });

    $(document).ready(function() {
        $('#btn-specifications').click(function() {
            $(this).siblings("#option-specifications").toggle();
            $("i", this).toggleClass("fa-caret-down fa-caret-up");
        });
    });
    $(".product-carousel").on('click', function(e) {
        e.preventDefault();
        let url = $(this).data('url');
        window.location.href = url;
    });

    const getShipping = (cep, type) => {
        let total = `{{ isset($total) ? $total : 0 }}`;
        axios.get(`{{ route('api.checkout.shipping') }}`, {
                params: {
                    cep,
                    total
                }
            })
            .then(response => {
                $('.shipping').html('');
                // `<div class="box-loading">
                //     <img src="{{ asset('img/loading.png') }}" alt="Carregando">
                // </div>`
                if (type == 'product') {
                    constructList(response.data[0]);
                    constructList(response.data[1]);
                } else {
                    constructRadio(response.data[0]);
                    constructRadio(response.data[1]);
                }
            })
            .catch(error => {
                console.log(error);
                return null;
            });
    }
    const constructRadio = (data) => {
        let value = JSON.stringify(data);
        let divShipping = $("<div>", {
            class: "form-check"
        });
        let inputShipping = $("<input>", {
            class: "form-check-input",
            type: "radio",
            name: "shipping",
            value: value
        });
        let labelShipping = $("<label>", {
            class: "form-check-label",
            text: `R$ ${data.shipping_price} - ${data.delivery_time} dias - ${data.type}`
        });
        divShipping.append(labelShipping);
        divShipping.append(inputShipping);
        $('.shipping').append(divShipping);
    }
    const constructList = (data) => {
        let liShipping = $("<li>");
        let spanValue = $("<span>", {
            class: "value",
            text: `R$ ${data.shipping_price}`
        });
        let spanDeadline = $("<span>", {
            class: "deadline",
            text: `${data.delivery_time} dias`
        });
        let spanName = $("<span>", {
            class: "name",
            text: data.type
        });
        liShipping.append(spanValue);
        liShipping.append(spanDeadline);
        liShipping.append(spanName);
        $('.table-freight').append(liShipping);
    }

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#street").val("");
        $("#district").val("");
        $("#city").val("");
        $("#state").val("");
    }

    function realToFloat(amount) {
        amount = amount.replace(/\./g, "");
        amount = amount.replace(",", ".");
        amount = parseFloat(amount);
        return amount;
    }

    function floatToReal(n, c, d, t) {
        c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ?
            "-" :
            "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(
            n -
            i).toFixed(c).slice(2) : "");
    }

</script>
@yield('scripts')
