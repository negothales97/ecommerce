<div id="menu-mobile">
    <div id="close-menu"><img src="{{ asset('img/arrow-left.png') }}" alt="Icon">Fechar menu</div>
    <div id="main-menu-mobile">
        <div id="menu-padding">
            <button><img src="{{ asset('img/atacado.png') }}" alt="Icon">Atacado</button>
            <button><img src="{{ asset('img/conta.png') }}" alt="Icon">Minha conta</button>
            <button><img src="{{ asset('img/lojas.png') }}" alt="Icon">Lojas</button>
            <button onclick="window.location.href='{{ route('contact') }}'"><img src="{{ asset('img/fone.png') }}"
                    alt="Icon">Fale conosco</button>
            <button><img src="{{ asset('img/trabalhe.png') }}" alt="Icon">Trabalhe conosco</button>
        </div>
        <p>Escolha uma categoria</p>
        @foreach ($mainCategories as $category)
            <button class="category menu-mobile-category" data-main_url="{{ route('category', ['slug' => $category->slug]) }}"
                data-url="{{ route('api.category.show', ['resourceId' => $category]) }}">{{ $category->name }}
                <img src="{{ asset('img/arrow-right.png') }}" alt="Icon">
            </button>
        @endforeach
    </div>
    <div id="submenu-mobile" style="display: none;">
        <div id="back-menu" onclick="hideSubmenuMobile();"><img src="{{ asset('img/arrow-left.png') }}"
                alt="Icon">Retornar</div>
        <h5 id="title-category"></h5>
        <div class="categories">
        </div>
    </div>
</div>
