@php
$cart = session()->get('cart');
$countCart = isset($cart) ? count($cart) : 0;
@endphp
<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-2">
                <a href="{{ url('/') }}"><img id="logo" src="{{ asset('img/logo.png') }}"></a>
            </div>
            <div class="col-5">
                <div class="box-search">
                    <form action="{{ route('search') }}">
                        <input placeholder="Procure seu produto..." id="search_string" name="search_string"
                            value="{{ request('search_string') }}">
                        <button type="submit"><i class="icon-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="col-4">
                <div id="box-user">
                    <i class="icon-user"></i>
                    <a href="{{ url('customer/login') }}">
                        @auth('customer')
                            <strong>{{ auth()->guard('customer')->user()->name }}</strong>
                        @else
                            <strong>Entre</strong><br>ou cadastre-se

                        @endauth
                    </a>
                </div>
                <div id="box-bag">
                    <i class="icon-bag"></i>
                    <a href="{{ route('cart.index') }}"><strong>Sacola</strong><br>{{ $countCart }} itens na sacola</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row-menu">
        <div class="container">
            <div class="row">
                <div class="col-sm-11 offset-sm-1">
                    <ul id="main-menu">
                        <li><a href="#"><strong>Feminino</strong></a></li>
                        <li><a href="#">Atacado</a></li>
                        <li><a class="{{request()->is('lojas') ? 'active' : ''}}"  href="#">Lojas</a></li>
                        <li><a class="{{request()->is('contato') ? 'active' : ''}}" href="{{route('contact')}}">Fale Conosco</a></li>
                        <li><a class="{{request()->is('trabalhe-conosco') ? 'active' : ''}}"  href="#">Trabalhe Conosco</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

<header id="header-mobile">
    <div class="container">
        <div class="row">
            <div class="col-2">
                <div class="btn-toggle">
                    <div class="row-menu-toggle"></div>
                    <div class="row-menu-toggle-small"></div>
                    <div class="row-menu-toggle"></div>
                    <div class="row-menu-toggle-small"></div>
                </div>
            </div>
            <div class="col-8">
                <a href="{{ url('/') }}"><img id="logo" src="{{ asset('img/logo.png') }}"></a>
            </div>
            <div class="col-2">
                <div id="box-bag-mobile">
                    <i class="icon-bag"></i>
                    <div class="items-bag"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="box-search">
                    <form>
                        <input placeholder="Procure seu produto...">
                        <button type="submit"><i class="icon-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('customer.includes.mobile')
</header>

<div id="overlay"></div>
