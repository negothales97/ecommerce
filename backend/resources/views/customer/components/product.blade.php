<div class="box-product">
    <div class="box-content-product product-carousel" data-url="{{route('product', ['slug' => $product->slug])}}">
        @if ($product->tag_id !== null)
            <div class="status">
                <p>{{ $product->tag->name }}</p>
            </div>
        @endif
        @if ($product->mainProductImage())
            <img src="{{ asset('uploads/products/original/' . $product->mainProductImage()->file) }}" alt="Modelo" class="img-search">
        @else
            <img src="{{ asset('img/slider.jpg') }}" alt="Modelo" class="img-search">
        @endif
        <div class="content-product">
            <p>{{$product->name}}</p>
            @if ($product->promotional_price == 0.0)
                <strong>R${{ convertMoneyUsaToBrazil($product->price) }}</strong>
            @else
                <strong>R${{ convertMoneyUsaToBrazil($product->promotional_price) }}</strong>
                <strike>R${{ convertMoneyUsaToBrazil($product->price) }}</strike>
            @endif
            @if ($product->promotional_price >= $paymentConfiguration->minimum_value_credit_card || $product->price >= $paymentConfiguration->minimum_value_credit_card)

            <small>
                {{ $paymentConfiguration->max_interest }}x de
                R${{ convertMoneyUsaToBrazil(($product->promotional_price == 0.0 ? $product->price : $product->promotional_price) / $paymentConfiguration->max_interest ) }}
                sem juros
            </small>
            @endif
        </div>
    </div>
</div>
