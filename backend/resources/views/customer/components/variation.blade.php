@foreach ($variations as $variation)
    <div class="row">
        <div class="col-sm-12">
            <div class="title-register">
                <span>Escolha {{ $variation->name }}</span>
            </div>
            <div class="box-option-product-with-title">
                @foreach ($variationOptions as $option)
                    @if ($option->variation_id == $variation->id)
                        <label for="option-{{ $option->id }}">
                        <input type="radio" name="variation-{{ $variation->id }}" value="{{$option->id}}" id="option-{{ $option->id }}" required>
                            <p>{{ $option->name }}</p>
                        </label>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endforeach
