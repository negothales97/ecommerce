<section class="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-about">
                    <h2>Sobre a marca</h2>
                    <p>
                        {!! $configuration->about !!}
                    </p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="newsletter">
                    <h2>Receba promoções<br> e novidades</h2>
                    <form action="{{ route('api.newsletter') }}" method="post">
                        @csrf
                        <input type="email" name="email" placeholder="Seu melhor e-mail" required>
                        <button type="submit"><img src="{{ asset('img/send.png') }}" alt="Icon"></button>
                        <label class="checkbox">Concordo com a política de privacidade
                            <input type="checkbox" name="policy" required>
                            <span class="checkmark"></span>
                        </label>
                    </form>
                    <div class="box-medias">
                        <p>Siga-nos</p>
                        @if ($configuration->facebook)
                            <a href="{{ $configuration->facebook }}" target="_blank"><img
                                    src="{{ asset('img/facebook.png') }}" alt="Facebook"></a>
                        @endif
                        @if ($configuration->instagram)
                            <a href="{{ $configuration->instagram }}" target="_blank"><img
                                    src="{{ asset('img/instagram.png') }}" alt="Instagram"></a>
                        @endif
                        @if ($configuration->youtube)
                            <a href="{{ $configuration->youtube }}" target="_blank"><img
                                    src="{{ asset('img/instagram.png') }}" alt="Instagram"></a>
                        @endif
                        @if ($configuration->linkedin)
                            <a href="{{ $configuration->linkedin }}" target="_blank"><img
                                    src="{{ asset('img/instagram.png') }}" alt="Instagram"></a>
                        @endif
                        @if ($configuration->tiktok)
                            <a href="{{ $configuration->tiktok }}" target="_blank"><img
                                    src="{{ asset('img/instagram.png') }}" alt="Instagram"></a>
                        @endif
                        @if ($configuration->pinterest)
                            <a href="{{ $configuration->pinterest }}" target="_blank"><img
                                    src="{{ asset('img/instagram.png') }}" alt="Instagram"></a>
                        @endif
                        @if ($configuration->twitter)
                            <a href="{{ $configuration->twitter }}" target="_blank"><img
                                    src="{{ asset('img/instagram.png') }}" alt="Instagram"></a>
                        @endif
                        <a href="#">@keepjeans</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
