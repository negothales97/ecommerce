<div class="row">
    <?php
    $numOfCols = 3;
    $rowCount = 0;
    $bootstrapColWidth = 12 / $numOfCols;
    ?>
    @foreach ($products as $product)
        <div class="col-sm-{{ $bootstrapColWidth }} col-6">
            @component('customer.components.product', ['product' => $product])
            @endcomponent
        </div>
        <?php $rowCount++ ?>
        @if ($rowCount % $numOfCols == 0)
</div>
<div class="row">
    @endif
    @endforeach
</div>
