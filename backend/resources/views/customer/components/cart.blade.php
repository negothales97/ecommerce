<div class="row">
    <div class="col-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col" style="width: 95px;"></th>
                    <th scope="col">Nome</th>
                    <th scope="col" style="width: 150px;">Qtd.</th>
                    <th scope="col" style="width: 150px;">Preço Unitário</th>
                    <th scope="col" style="width: 150px;">Subtotal</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @forelse($items as $key => $item)
                    <tr>
                        <td class="text-center align-middle py-2">
                            <a href="{{ route('product', ['slug' => $item['product']->slug]) }}">
                                @if ($item['product']->mainProductImage())
                                    <img class="image-product-cart"
                                        src="{{ asset('uploads/products/original/' . $item['product']->mainProductImage()->file) }}"
                                        alt="Modelo">
                                @else
                                    <img class="image-product-cart" src="{{ asset('img/slider.jpg') }}" alt="Modelo">
                                @endif
                            </a>
                        </td>
                        <td class="text-center align-middle py-0">
                            <a href="{{ route('product', ['slug' => $item['product']->slug]) }}">
                                {{ $item['product']->name }}
                            </a>
                        </td>
                        <td class="text-center align-middle py-0">
                            <form action="{{ route('cart.update.product') }}" class="form-qty-{{ $item['product_id'] }}">
                                <input type="text" name="quantity[{{ $item['cart_key'] }}]" min="1"
                                    class="form-control input-qty" data-product_id="{{ $item['product_id'] }}"
                                    data-qty="{{ $item['quantity'] }}" value="{{ $item['quantity'] }}" style="width: 90px;">
                            </form>
                        </td>
                        @php($price = $item['product']->promotional_price == 0.0 ? $item['product']->price :
                            $item['product']->promotional_price)
                            <td class="text-center align-middle py-0">
                                R$ {{ convertMoneyUSAToBrazil($price) }}
                            </td>
                            <td class="text-center align-middle py-0">
                                R$ {{ convertMoneyUSAToBrazil($price * $item['quantity']) }}
                            </td>
                            <td class="text-center align-middle py-0">
                                <div class="btn-group btn-group-sm">
                                    <a href="{{ route('cart.delete.product', ['id' => $item['cart_key']]) }}" title="Excluir"
                                        class="btn btn-danger act-delete">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="6">Nenhum produto no carrinho</td>
                            </tr>
                        @endforelse
                        <tr>
                            <td colspan="4"></td>
                            <td><b>Subtotal:</b></td>
                            <td class="subtotal"></td>
                        </tr>
                        <tr>
                            <td colspan="4"></td>
                            <td><b>Frete:</b></td>
                            <td class="freight"></td>
                        </tr>
                        <tr>
                            <td colspan="4"></td>
                            <td><b>Total:</b></td>
                            <td class="total"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
