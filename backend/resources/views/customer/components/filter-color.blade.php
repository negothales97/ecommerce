<p>Cor</p>
<div class="box-filter-option">
    @foreach ($colors as $color)
        <label class="checkbox">
            <div>
                <img class="color-filter" src="{{ asset('uploads/colors/thumbnail') }}/{{ $color->file }}">
            </div>{{ $color->name }}
        <input type="checkbox" name="color" class="color-filter" value="{{$color->id}}">
            <span class="checkmark"></span>
        </label>
    @endforeach
</div>
