<p>{{ $filter->name }}</p>
<div class="box-filter-option box-variation" id="variation-{{$filter->id}}">
    @foreach ($filter->options as $option)
        <label class="checkbox" for="option-{{ $option->id }}">{{ $option->name }}
            <input type="checkbox" class="variation-filter" name="variation-{{ $filter->id }}" value="{{ $option->id }}"
                id="option-{{ $option->id }}">
            <span class="checkmark"></span>
        </label>
    @endforeach

</div>
