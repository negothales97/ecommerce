@if($colors->count())
<div class="row">
    <div class="col-sm-12">
        <div class="title-register">
            <span>Escolha a cor</span>
        </div>
        <div class="box-option-product-with-title">
            @foreach ($colors as $color)
                <label for="color-{{ $color->id }}">
                    <input type="radio" name="color" id="color-{{ $color->id }}" value="{{$color->id}}" required>
                    <img src="{{ asset('uploads/colors/thumbnail')."/". $color->file }}" alt="{{ $color->name }}">
                </label>
            @endforeach
        </div>
    </div>
</div>
@endif