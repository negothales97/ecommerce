@extends('customer.templates.default')
@section('content')

    <section class="content">
        <section class="checkout">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3">
                        <div class="box-register">
                            <p>Para continuar, informe seu e-mail</p>
                            <form action="{{ url('customer/login') }}" id="login-email" method="post">
                                @csrf
                                <input type="hidden" name="email">
                                <input type="hidden" name="checkout" value="true">
                                <input type="hidden" name="password">
                            </form>
                            <form method="post" id="validate-email">
                                @csrf
                                <input type="hidden" name="validate" id="input-validate">
                                <div class="form-group">
                                    <input type="email" name="email" id="input-email" placeholder="Seu e-mail" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="input-password" class="display-none"
                                        placeholder="Informe a senha">
                                </div>
                                <button type="submit">Continuar</button>
                            </form>
                        </div>
                        <h5>Usamos seu e-mail de forma segura para</h5>
                        <ul>
                            <li><img src="{{ asset('img/check.png') }}" alt="Check">Identificar seu perfil</li>
                            <li><img src="{{ asset('img/check.png') }}" alt="Check">Notificar sobre o andamento do seu
                                pedido</li>
                            <li><img src="{{ asset('img/check.png') }}" alt="Check">Gerenciar seu histórico de compras</li>
                            <li><img src="{{ asset('img/check.png') }}" alt="Check">Acelerar o preechimento de suas
                                informações</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </section>

@endsection
@section('scripts')
    <script type="text/javascript">
        $('#validate-email').on('submit', function(e) {
            e.preventDefault();
            $('.has-error').removeClass('has-error');
            let email = $("#input-email").val();
            let validate = $("#input-validate").val();
            if (validate == 'true') {
                $('#login-email input[name=email]').val(email);
                $('#login-email input[name=password]').val($("#input-password").val());
                $('#login-email').submit();
            } else {
                makeConsult(email);
            }
        });
        const makeConsult = async (email) => {
            let customer = await validateEmail(email);
            if (customer) {
                $('#input-validate').val('true');
                $("#input-password").slideDown();
            }
        }
        const validateEmail = async (email) => {
            return await axios.get(`{{ route('api.checkout.consult') }}`, {
                    params: {
                        email
                    }
                })
                .then(response => {
                    return response.data;
                })
                .catch(error => {
                    let {
                        status,
                        data
                    } = error.response;

                    if (status == 422) {
                        for (error in data.errors) {
                            let elementError = $(`#input-${error}`);
                            elementError.addClass('has-error');
                        }
                        return null;
                    }
                    sessionStorage.setItem('email', email);
                    window.location.href = `{{ route('cart.checkout.index') }}`;
                    return null;
                });
        }

    </script>
@endsection
