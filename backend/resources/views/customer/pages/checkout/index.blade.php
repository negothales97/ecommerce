@extends('customer.templates.default')
@section('content')

    <section class="content">
        <section class="checkout">
            <div class="container">
                @component('customer.components.cart', ['items' => $items])
                @endcomponent

                <form action="{{ route('cart.checkout.store') }}" method="post">
                    @csrf
                    @php
                    $customer = auth()->guard('customer')->user() ?? collect();
                    @endphp
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="title-register">
                                <span>Cadastro</span>
                            </div>

                            <div class="box-register box-register-with-title">
                                <x-input name="email" value="{{ $customer->email }}" label="E-mail"
                                    attribute="required {{ $customer != null ? 'readonly' : '' }}" />
                                @auth
                                    <div class="form-group">
                                        <label for="input-password">Criar uma senha</label>
                                        <input type="password"
                                            class="form-control {{ $errors->has('password') ? 'has-error' : '' }}"
                                            id="input-password" name="password" required autocomplete="off">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="input-password_confirmation">Confirmar senha</label>
                                        <input type="password"
                                            class="form-control {{ $errors->has('password') ? 'has-error' : '' }}"
                                            id="input-password_confirmation" name="password_confirmation" required
                                            autocomplete="off">
                                    </div>
                                @endauth
                                <x-input name="name" value="{{ $customer->name }}" label="Nome Completo"
                                    attribute="required {{ $customer != null ? 'readonly' : '' }}" />
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="input-fisica" name="document_type" checked
                                        class="custom-control-input" value="cpf">
                                    <label class="custom-control-label" for="input-fisica" value="cpf"
                                        {{ old('document_type', $customer->document_type) == 'cpf' ? 'checked' : '' }}>Pessoa
                                        física</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="input-juridica" value="cnpj" name="document_type"
                                        class="custom-control-input">
                                    <label class="custom-control-label" for="input-juridica" value="cnpj"
                                        {{ old('document_type', $customer->document_type) == 'cnpj' ? 'checked' : '' }}>Pessoa
                                        jurídica</label>
                                </div>

                                <div class="form-group" id="form-cpf">
                                    <label for="document_number">CPF</label>
                                    <input type="text" value="{{ old('document_number', $customer->document_number) }}"
                                        class="form-control input-cpf {{ $errors->has('document_number') ? 'has-error' : '' }}"
                                        id="document_number" name="document_number">
                                    @if ($errors->has('document_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('document_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group display-none" id="form-cnpj">
                                    <label for="cnpj">CNPJ</label>
                                    <input type="text"
                                        class="form-control input-cnpj {{ $errors->has('document_number') ? 'has-error' : '' }}"
                                        value="{{ old('document_number', $customer->document_number) }}" id="cnpj"
                                        name="document_number" disabled>
                                    @if ($errors->has('document_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('document_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <x-input name="birthday" clazz="input-date" value="{{ $customer->birthday }}"
                                    label="Data de Nascimento"
                                    attribute="required {{ $customer != null ? 'readonly' : '' }}" />
                                <x-input name="cellphone" clazz="input-phone" value="{{ $customer->cellphone }}"
                                    label="Celular" attribute="required {{ $customer != null ? 'readonly' : '' }}" />

                                {{-- <div class="form-group">
                                    <label for="input-cellphone">Celular</label>
                                    <input type="text"
                                        class="form-control input-phone {{ $errors->has('cellphone') ? 'has-error' : '' }}"
                                        value="{{ old('cellphone', $customer->cellphone) }}" required id="input-cellphone"
                                        name="cellphone">
                                    @if ($errors->has('cellphone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('cellphone') }}</strong>
                                        </span>
                                    @endif
                                </div> --}}
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="title-register">
                                <span>Entrega</span>
                            </div>
                            <div class="box-register box-register-with-title">
                                <div class="form-group">
                                    <label for="cep">CEP</label>
                                    <input type="text"
                                        class="form-control input-cep {{ $errors->has('zip_code') ? 'has-error' : '' }}"
                                        value="{{ old('zip_code') }}" id="cep" required name="zip_code">
                                    @if ($errors->has('zip_code'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('zip_code') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-12 pt-0">Escolha a forma de entrega</legend>
                                        <div class="col-sm-12">
                                            <div class="shipping">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group">
                                    <label for="street">Endereço</label>
                                    <input type="text" class="form-control {{ $errors->has('street') ? 'has-error' : '' }}"
                                        value="{{ old('street') }}" id="street" required name="street">
                                    @if ($errors->has('street'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('street') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="number">Número</label>
                                    <input type="text" class="form-control {{ $errors->has('number') ? 'has-error' : '' }}"
                                        id="number" name="number" required>
                                    @if ($errors->has('number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="complement">Complemento</label>
                                    <input type="text"
                                        class="form-control {{ $errors->has('complement') ? 'has-error' : '' }}"
                                        value="{{ old('complement') }}" id="complement" name="complement">
                                    @if ($errors->has('complement'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('complement') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="district">Bairro</label>
                                    <input type="text"
                                        class="form-control {{ $errors->has('district') ? 'has-error' : '' }}"
                                        value="{{ old('district') }}" id="district" required name="district">
                                    @if ($errors->has('district'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('district') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="state">Estado</label>

                                    <select name="state" id="state"
                                        class="form-control {{ $errors->has('state') ? 'has-error' : '' }}" required>
                                        <option selected disabled>Selecione..</option>
                                        @foreach ($states as $state)
                                            <option value="{{ $state->sigla }}"
                                                {{ old('state') == $state->id ? 'selected' : '' }}>{{ $state->sigla }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('state'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="city">Cidade</label>
                                    <input type="text" class="form-control {{ $errors->has('city') ? 'has-error' : '' }}"
                                        value="{{ old('city') }}" id="city" name="city" required>
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="title-register">
                                <span>Pagamento</span>
                            </div>
                            <div class="box-register box-register-with-title">
                                <div>
                                    <ul class="payment-methods">
                                        <li class="payment-method credit_card">
                                            <input name="payment_method" type="radio" id="credit_card" value="credit_card">
                                            <label for="credit_card">Cartão de Crédito</label>
                                        </li>

                                        <li class="payment-method boleto">
                                            <input name="payment_method" type="radio" id="boleto" value="boleto">
                                            <label for="boleto">Boleto Bancário</label>
                                        </li>
                                        @if ($errors->has('payment_method'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('payment_method') }}</strong>
                                            </span>
                                        @endif
                                    </ul>
                                </div>
                                <div class="credit_card-box display-none">
                                    <img id="card-checkout" src="{{ asset('img/card-checkout.png') }}" alt="Cartão">
                                    <div class="form-group">
                                        <label for="card_number">Número do cartão de crédito</label>
                                        <input type="text"
                                            class="form-control {{ $errors->has('card_number') ? 'has-error' : '' }}"
                                            id="card_number" name="card_number" placeholder="0000 0000 0000 0000">
                                        @if ($errors->has('card_number'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('card_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="validity">Validade</label>
                                        <input type="text" class="form-control input-validity" id="validity" name="validity"
                                            placeholder="MM/AA">

                                        @if ($errors->has('validity'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('validity') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="card_holder_name">Nome escrito no cartão</label>
                                        <input type="text"
                                            class="form-control {{ $errors->has('card_holder_name') ? 'has-error' : '' }}"
                                            id="card_holder_name" name="card_holder_name">
                                        @if ($errors->has('card_holder_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('card_holder_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="cvv">Código de segurança</label>
                                        <input type="text" class="form-control {{ $errors->has('cvv') ? 'has-error' : '' }}"
                                            id="cvv" name="cvv" placeholder="CVV">
                                        @if ($errors->has('cvv'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('cvv') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="parcels">Número de parcelas</label>
                                        <select class="form-control {{ $errors->has('parcels') ? 'has-error' : '' }}"
                                            id="parcels" name="parcels">
                                            @for ($i = 1; $i < 7; $i++)
                                                <option value="{{ $i }}">{{ $i }}x de
                                                    R${{ convertMoneyUsaToBrazil($total / $i) }} sem juros</option>
                                            @endfor
                                        </select>
                                        @if ($errors->has('parcels'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('parcels') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="boleto-box display-none">
                                    <div class="total"></div>

                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="delivery" checked>
                                    <label class="custom-control-label" for="delivery">Endereço de entrega igual ao de
                                        cobrança</label>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit">Finalizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            let email = sessionStorage.getItem('email');
            $("#email").val(email)
        });
        $('input[name=document_type]').on('change', function() {
            let documentType = $("input[name=document_type]:checked").val();
            console.log(documentType);
            if (documentType == "cpf") {
                $("#form-cnpj").addClass('display-none');
                $("#form-cpf").removeClass('display-none');

                $("#cnpj").attr('disabled', true);
                $("#cpf").attr('disabled', false);
            } else {
                $("#form-cpf").addClass('display-none');
                $("#form-cnpj").removeClass('display-none');

                $("#cnpj").attr('disabled', false);
                $("#cpf").attr('disabled', true)
            }
        });

        $(document).ready(function() {
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if (validacep.test(cep)) {
                        getShipping(cep);

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#street").val("...");
                        $("#district").val("...");
                        $("#city").val("...");
                        $("#state").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#street").val(dados.logradouro);
                                $("#district").val(dados.bairro);
                                $("#city").val(dados.localidade);
                                $("#state").val(dados.uf);

                                $("#street").attr("readonly", true);
                                $("#district").attr("readonly", true);
                                $("#city").attr("readonly", true);
                                $("#state").attr("readonly", true);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

        $(document).ready(function() {
            $('.subtotal').text("R$ {{ convertMoneyUsaToBrazil($total) }}");
            $('.total').text(`R$ {{ convertMoneyUsaToBrazil($total) }}`);
        });
        $(document).on('change', '.shipping input', function() {
            let data = $(this).val();
            let shipping = JSON.parse(data);
            $('.freight').text(`R$ ${shipping.shipping_price}`);
            $('.total').text(`R$ ${shipping.new_total}`);
            let total = realToFloat(shipping.new_total);

            $('#parcels').html('');
            for (let i = 1; i <= "{{ $paymentConfiguration->max_interest }}"; i++) {
                let parcel = total / i;
                $('#parcels').append(
                    `<option value="${parcel}">${i}x de R$ ${floatToReal(parcel)} sem juros</option>`);
            }
        });

        $('.payment-method input').on('change', function(e) {
            e.preventDefault();

            let payment_method = $(this).val();
            if (payment_method == 'credit_card') {
                $(`.credit_card-box`).removeClass('display-none');
                $(`.boleto-box`).addClass('display-none');

            } else {
                $(`.credit_card-box`).addClass('display-none');
                $(`.boleto-box`).removeClass('display-none');
            }
        });

    </script>
@endsection
