@extends('customer.templates.default')
@section('content')

<section class="content">
    <section class="checkout">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3">
                    <div class="box-register box-order-placed">
                        <img src="{{ asset('img/tick.png') }}" alt="Check">
                        <h4>Pedido realizado</h4>
                        <span>#{{session('order_id')}}</span>
                        <div class="box-steps">
                            <img src="{{ asset('img/step1.png') }}" alt="Icon">
                            <small id="waiting">Aguardando confirmação de pagamento</small>
                            <img class="next" src="{{ asset('img/next.png') }}" alt="Icon">
                        </div>
                        <div class="box-steps">
                            <img src="{{ asset('img/step2.png') }}" alt="Icon">
                            <small>Emitindo sua nota fiscal e embalando</small>
                            <img class="next" src="{{ asset('img/next.png') }}" alt="Icon">
                        </div>
                        <div class="box-steps">
                            <img id="step3" src="{{ asset('img/step3.png') }}" alt="Icon">
                            <small>Pedido enviado para o seu endereço</small>
                        </div>
                        <div id="next-step">
                            <h5>Próximos passos</h5>
                            <p>
                                1. Estamos processando seu pagamento e assim que ele for confirmado te notificaremos por e-mail.<br><br>

                                2. Com o pagamento aprovado, seu pedido sairá para entrega e você receberá um link para acompanhá-lo.<br><br>

                                3. Assim que o pedido for entregue, você receberá um link para avaliar o produto e o atendimento recebido.
                            </p>
                        </div>
                    </div>
                <button class="btn-orange" onclick="window.location.href='{{url('/')}}'">Voltar para o site</button>
                </div>
            </div>
        </div>
    </section>
</section>

@endsection