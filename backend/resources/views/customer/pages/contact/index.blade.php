@extends('customer.templates.default')
@section('content')
    <section class="content">
        <section class="checkout">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-sm-4">
                        <div class="title-register">
                            <span>Fale Conosco</span>
                        </div>
                        <div class="box-register box-register-with-title">
                            <form method="post" action="{{ route('contact') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Nome</label>
                                    <input type="text" class="form-control" id="name" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input type="email" class="form-control" id="email" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Telefone</label>
                                    <input type="text" class="form-control" id="phone" name="phone" required>
                                </div>
                                <div class="form-group">
                                    <label for="subject">Assunto</label>
                                    <input type="text" class="form-control input-phone" id="subject" name="subject" required>
                                </div>
                                <div class="form-group">
                                    <label for="content">Mensagem</label>
                                    <textarea name="content" id="content" class="form-control" rows="5" required></textarea>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <button type="submit">Enviar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
