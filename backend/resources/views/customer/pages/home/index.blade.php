@extends('customer.templates.default')
@section('content')

    {{-- <section class="banner"></section> --}}

    <section class="options">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <img src="{{ asset('img/delivery.png') }}" alt="Icon">
                    <p><strong>Frete grátis</strong> nas compras acima de R$250,00</p>
                </div>
                <div class="col-sm-6">
                    <img src="{{ asset('img/card.png') }}" alt="Icon">
                    <p><strong>Até {{ $paymentConfiguration->max_interest }}x sem juros</strong>
                        nas compras acima de
                        R${{ convertMoneyUsaToBrazil($paymentConfiguration->minimum_value_credit_card) }}</p>
                </div>
            </div>
        </div>
    </section>

    <section class="options-mobile">
        <div class="container">
            <div class="box-options-mobile">
                <h5>FRETE GRÁTIS</h5>
                <p>Para compras acima de R$250,00</p>
            </div>
        </div>
    </section>

    <section class="content">
        <section class="promotions">
            <div class="container">
                <h2>PROMOÇÕES</h2>
                <div class="slider">
                    <button class="prev-carousel"><i class="fas fa-chevron-left"></i></button>
                    <button class="next-carousel"><i class="fas fa-chevron-right"></i></button>
                    <div class="slider-carousel">
                        @foreach ($promotions as $promotion)
                            @component('customer.components.carousel', ['product' => $promotion])
                            @endcomponent
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="spotlight">
            <div class="container">
                <h2>CATEGORIAS EM DESTAQUE</h2>
                <div class="row">
                    <?php
                    $numOfCols = 6;
                    $rowCount = 0;
                    $bootstrapColWidth = 12 / $numOfCols;
                    ?>
                    @foreach ($featuredCategories as $featuredCategory)

                        <div class="col-sm-{{ $bootstrapColWidth }} col-6">
                            <div class="box-spotlight"
                                data-url="{{ route('category', ['slug' => $featuredCategory->slug]) }}">
                                <h5>{{ $featuredCategory->name }}</h5>
                            </div>
                        </div>
                        @php
                            $rowCount++
                        @endphp
                            @if ($rowCount % $numOfCols == 0)
                    </div>
                    <div class="row">
                        @endif
                        @endforeach

                    </div>
                </div>
            </section>
            <section class="wholesale">
                <div class="container">
                    <div class="box-wholesale">
                        <h3>Deseja comprar no <strong>atacado?</strong></h3>
                    <button onclick="window.location.href='{{route('contact')}}'">Fale conosco</button>
                    </div>
                </div>
            </section>
            <section class="news">
                <div class="container">
                    <h2>NOVIDADES</h2>
                    <div class="slider">
                        <button class="prev-carousel-news"><i class="fas fa-chevron-left"></i></button>
                        <button class="next-carousel-news"><i class="fas fa-chevron-right"></i></button>
                        <div class="slider-carousel-news">
                            @foreach ($novelties as $novelty)
                                @component('customer.components.carousel', ['product' => $novelty])
                                @endcomponent
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
            @if(false)
            <section class="lightning-promotion">
                <div class="container">
                    <h2>PROMOÇÃO RELÂMPAGO</h2>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="box-promotion">
                                <img src="{{ asset('img/hour.png') }}" alt="Icon">
                                <div class="chronometer">
                                    <p>12</p>
                                    <p class="two-points">:</p>
                                    <small>horas</small>
                                </div>
                                <div class="chronometer">
                                    <p>15</p>
                                    <p class="two-points">:</p>
                                    <small>minutos</small>
                                </div>
                                <div class="chronometer">
                                    <p>02</p>
                                    <small>segundos</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider">
                        <button class="prev-carousel-promotion"><i class="fas fa-chevron-left"></i></button>
                        <button class="next-carousel-promotion"><i class="fas fa-chevron-right"></i></button>
                        <div class="slider-carousel-promotion">

                            @foreach ($fastPromotions as $fastPromotion)
                                @component('customer.components.carousel', ['product' => $fastPromotion])
                                @endcomponent
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
            @endif
            @component('customer.components.about')
            @endcomponent
        </section>

        @component('customer.components.social-media')
        @endcomponent

    @endsection
@section('scripts')
    <script type="text/javascript">
        $('.box-spotlight').on('click', function(e) {
            e.preventDefault();
            let url = $(this).data('url');
            window.location.href = url;
        });

    </script>
@endsection
