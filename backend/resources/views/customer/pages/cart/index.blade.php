@extends('customer.templates.default')
@section('content')
    <section class="content">
        <section class="promotions">
            <div class="container">
                <h2>Carrinho</h2>
                @component('customer.components.cart', ['items' => $items])
                @endcomponent

                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-12">
                        <button type="button" class="btn-left-more-products"
                            onclick="window.location.href='{{ route('search') }}';"><i class="fa fa-arrow-circle-left"
                                aria-hidden="true"></i> Continuar comprando</button>
                        <button type="button" class="btn-right-cart"
                            onclick="window.location.href='{{ route('cart.checkout.email') }}';">
                            Finalizar Compra <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
        </section>
    </section>
    @component('customer.components.social-media')
    @endcomponent

@endsection
@section('scripts')
    <script type="text/javascript">
        $(".input-qty").on('blur', function(e) {
            let newQty = $(this).val();
            let oldQty = $(this).data('qty');
            let product_id = $(this).data('product_id');
            if (newQty != oldQty) {
                $(`.form-qty-${product_id}`).submit();
            }
        });
        $(document).ready(function() {
            $('.subtotal').text("R$ {{ convertMoneyUsaToBrazil($total) }}");
            $('.total').text(`R$ {{ convertMoneyUsaToBrazil($total) }}`);
        });

    </script>
@endsection
