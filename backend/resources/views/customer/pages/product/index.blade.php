@extends('customer.templates.default')
@section('content')

    <section class="content content-pages">
        <section class="category">
            <div class="container">
                <nav class="breadcrumb-pages" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        @component('customer.components.breadcrumb',['product'=> $product])
                       
                        @endcomponent
                    </ol>
                    <h4>{{ $product->name }}</h4>
                </nav>
                <div class="row">
                    <div class="col-sm-8">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @forelse($product->images as $image)
                                    <div class="carousel-item img-zoom {{ $image->position == 1 ? 'active' : '' }}">
                                        <img class="img-product-carousel zoom"
                                            data-zoom-image="{{ asset('uploads/products/original/' . $image->file) }}"
                                            src="{{ asset('uploads/products/original/' . $image->file) }}" alt="Modelo">
                                    </div>
                                @empty
                                    <div class="carousel-item">
                                        <img class="img-product-carousel zoom" data-zoom-image="{{ asset('img/slider.jpg') }}"
                                            src="{{ asset('img/slider.jpg') }}" alt="Modelo">
                                    </div>
                                @endforelse
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <form action="{{ route('cart.store') }}" method="post">
                            @csrf
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            @component('customer.components.color', ['colors' => $colors])
                            @endcomponent
                            @component('customer.components.variation', ['variations' => $variations, 'variationOptions' =>
                                $variationOptions])
                            @endcomponent
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="title-register">
                                        <span>Escolha a quantidade</span>
                                    </div>
                                    <div class="box-option-product-with-title">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="input-qty" name="qty" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box-price">
                                        @if ($product->promotional_price == 0.0)
                                            <p>R$<span>R${{ convertMoneyUsaToBrazil($product->price) }}</span></p>
                                            <small>Em até 6x de R${{ convertMoneyUsaToBrazil($product->price / 6) }}</small>
                                        @else
                                            <p>R$<span>R${{ convertMoneyUsaToBrazil($product->promotional_price) }}</span>
                                            </p>
                                            <small>
                                                {{ $paymentConfiguration->max_interest }}x de
                                                R${{ convertMoneyUsaToBrazil($product->promotional_price == 0.0 ? $product->price : $product->promotional_price / $paymentConfiguration->max_interest) }}
                                                sem juros
                                            </small>
                                        @endif
                                        <button class="btn-orange">Comprar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-options-toggle" id="btn-freight">
                                    Calcular frete
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </div>
                                <div class="box-options-toggle display-none" id="option-freight">
                                    <form>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control input-cep" id="freight"
                                                    placeholder="Digite seu CEP">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-light btn-freight">OK</button>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="table-freight">
                                        </ul>
                                    </form>
                                </div>
                                <div class="btn-options-toggle" id="btn-description">
                                    Descrição
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </div>
                                <div class="box-options-toggle display-none" id="option-description">
                                    <p>
                                        {{ $product->description }}
                                    </p>
                                </div>
                                <div class="btn-options-toggle" id="btn-size">
                                    Guia de medidas
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </div>
                                <div class="box-options-toggle display-none" id="option-size">
                                    <p>
                                        {{ $product->measurement_guide }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="relacionadas">
            <div class="container">
                <h2>RELACIONADOS</h2>
                <div class="slider">
                    <button class="prev-carousel-promotion"><i class="fas fa-chevron-left"></i></button>
                    <button class="next-carousel-promotion"><i class="fas fa-chevron-right"></i></button>
                    <div class="slider-carousel-promotion">

                        @foreach ($relatedProducts as $relatedProduct)
                            @component('customer.components.carousel', ['product' => $relatedProduct])
                            @endcomponent
                        @endforeach
                    </div>
                </div>
        </section>
        @component('customer.components.about')
        @endcomponent
    </section>

    @component('customer.components.social-media')
    @endcomponent


@endsection
@section('scripts')
<script type="text/javascript">
    $('.zoom').ezPlus({
        zoomType: 'lens',
        lensShape: 'round',
        containLensZoom: true,
        lensSize: 300
    });

    $('.btn-options-toggle').on('click', function(e) {
        e.preventDefault();
        let id = $(this).attr('id');
        id = id.replace("btn", "box");
        $(`#${id}`).slideToggle();
    });


    $(".btn-freight").click(function(e) {
        e.preventDefault();
        //Nova variável "cep" somente com dígitos.
        var cep = $('#freight').val().replace(/\D/g, '');
        //Verifica se campo cep possui valor informado.
        if (cep != "") {
            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;
            //Valida o formato do CEP.
            if (validacep.test(cep)) {
                getShipping(cep, 'product');
            } else {
                alert("Formato de CEP inválido.");
            }
        } else {
            alert("CEP não localizado");
        }
    });

</script>
@endsection
