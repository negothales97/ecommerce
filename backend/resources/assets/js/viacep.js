$(document).ready(function () {

    function cleanForm() {
        // Limpa valores do formulário de cep.
        $("#street").val("");
        $("#district").val("");
        $("#city").val("");
        $("#state").val("");
    }
    function showMessage(msg, type){
        if(type === "error"){
            toastr.error(msg);
        }else{
            toastr.success(msg);
        }
    }

    //Quando o campo cep perde o foco.
    $("#zip_code").blur(function () {

        var cep = $(this).val().replace(/\D/g, '');
        if (cep != "") {
            var validaCep = /^[0-9]{8}$/;
            if (validaCep.test(cep)) {
                //Preenche os campos com "..." enquanto consulta webservice.
                $("#street").val("...");
                $("#district").val("...");
                $("#city").val("...");
                $("#state").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#street").val(dados.logradouro);
                        $("#district").val(dados.bairro);
                        $("#city").val(dados.localidade);
                        $("#state").val(dados.uf);

                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        cleanForm();
                        showMessage("CEP não encontrado.", "error");
                    }
                });
            } //end if.
            else {
                cleanForm();
                showMessage("Formato de CEP inválido.", "error");
            }
        } //end if.
        else {
            cleanForm();
        }
    });
});