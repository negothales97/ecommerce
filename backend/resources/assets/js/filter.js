uri = uri.replace('#', '');
var load = true;
var page = 2;
var end = 0;
$(window).scroll(function () {
    if (end == 0) {
        if (load && ($(this).scrollTop() + $(this).height() > $("#list-product").height())) {
            renderPaginate(uri);
        }
    }
});

const renderPaginate = async (uri) => {
    load = false;
    let data = {};
    data.page = page;
    if (typeof (categoryId) != "undefined" && categoryId !== null) {
        data.category_id = categoryId;
    } else {
        data.search_string = $("#search_string").val();
    }

    data.order = $("#input-order").val();
    data.field = $("#input-field").val();
    data.colors = $("#input-colors").val();
    data.variations = $("#input-variations").val();


    let productsHtml = await getProductsPaginante(uri, data);
    $('#list-product').append(productsHtml);
    page++;
    setTimeout(function () {
        load = true;
    }, 300);
}

const getProductsPaginante = async (url, data = null) => {
    axios.interceptors.request.use((config) => {
        $('.box-loading').removeClass('display-none');
        return config;
    });
    return await axios.get(url, {
        params: data
    })
        .then(response => {
            $('.box-loading').addClass('display-none');
            return response.data;
        })
        .catch((error) => console.log(error));
}

$('.color-filter').on('change', function (e) {
    e.preventDefault();
    let colors = [];
    $('input[name=color]:checked').each(function() {
        colors.push($(this).val());
    });
    colors = JSON.stringify(colors);
    $('#input-colors').val(colors);
    
    initializeSearch();
});

$('.variation-filter').on('change', function (e) {
    e.preventDefault();
    let options = [];
    let variations = {};
    let box = $('.box-variation');
    box.map(function(){
        $(this).find('input:checked').each(function(){
            options.push($(this).val());
        });
        let id = this.id;
        variations[id] = options;
        options =[];
        return;
    });

    $('#input-variations').val(JSON.stringify(variations));
    
    initializeSearch();
});

$('.box-order-desktop a').on('click', function () {
    let order = $(this).data('order');
    let field = $(this).data('field');
    $('.box-order-desktop a').removeClass('active');
    $(this).addClass('active');

    $('#input-field').val(field);
    $('#input-order').val(order);
    initializeSearch();
});

const initializeSearch = ()=> {
    page = 1;
    $('#list-product').html('');
    renderPaginate(uri);
}