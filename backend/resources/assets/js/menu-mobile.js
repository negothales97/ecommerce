const axios = require('axios');

$(document).on('click', '.menu-mobile-category', function(e){
    e.preventDefault();
    let url = $(this).data('url');
    console.log(url);
    let mainUrl = $(this).data('main_url');
    showSubmenuMobile(url, mainUrl);
});

$('#back-menu').on('click', function(e){
    e.preventDefault();
    hideSubmenuMobile();
});


const showSubmenuMobile = async(url, mainUrl = null ) => {
    let category = await getItems(url);
    let {categories} = category;
    if(categories.length == 0){
        window.location.href=mainUrl;
        return;
    }
    
    document.getElementById("main-menu-mobile").style.display = "none";
    document.getElementById("close-menu").style.display = "none";
    document.getElementById("title-category").innerHTML = category.name;
    document.getElementById("submenu-mobile").style.display = "block";
    categories.forEach(subcategory => createViewSubcategories(subcategory, url));
};

const createViewSubcategories = async (subcategory, url)=> {
    let categoryDiv = $('.categories').html('');

    url = url.substring(0,url.indexOf("category") + 9) + subcategory.id;
    let category = await getItems(url);
    let mainUrl = category.url;
    let buttonEl = $('<button>', {
        class: "menu-mobile-category w-submenu",
        text: subcategory.name,
        "data-url": url,
        "data-main_url": mainUrl,
    });
    let imgEl = $("<img>", {
        src:"../img/arrow-right.png",
        alt: "Icon"
    });
    if(category.categories.length != 0)
        buttonEl.append(imgEl);
    
    categoryDiv.append(buttonEl);
}

const hideSubmenuMobile = async(url) => {
    document.getElementById("main-menu-mobile").style.display = "block";
    document.getElementById("close-menu").style.display = "block";
    document.getElementById("submenu-mobile").style.display = "none";
};
const getItems = async (url) => {
    return axios.get(url)
        .then((response) => {
            return response.data
        })
        .catch((error) => {
            console.log(error);
            // validateError(error.response);
        });
}