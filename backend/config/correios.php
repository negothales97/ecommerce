<?php

return [
    'cep_destino' => '04101300',
    'empresa' => env('CORREIOS_EMPRESA'),
    'senha' => env('CORREIOS_SENHA'),
];
