<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('shipping_type');
            $table->double('shipping_price');
            $table->integer('delivery_time');
            $table->double('subtotal');
            $table->double('total');
            $table->string('payment_status');
            $table->string('tracking_code')->nullable();
            $table->integer('status')->default(0);
            $table->string('boleto_url')->nullable();
            $table->string('transaction_id');
            
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
