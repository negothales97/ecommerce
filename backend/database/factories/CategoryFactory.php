<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->unique(true)->slug,
        'meta_title' => $faker->word,
        'meta_description' => $faker->text,
        "featured" => $faker->boolean($chanceOfGettingTrue = 50),
    ];
});
