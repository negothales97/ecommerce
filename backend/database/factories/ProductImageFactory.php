<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductImage;
use Faker\Generator as Faker;

$factory->define(ProductImage::class, function (Faker $faker) {
    $files = [
        "product-1.jpg",
        "product-2.jpg",
        "product-3.jpg",
    ];
    return [
        'product_id' =>$faker->unique(true)->numberBetween($min = 1, $max =100),
        'file' => ($files[$faker->unique()->numberBetween($min = 0, $max =2)]),
        'position'=> 1
    ];
});
