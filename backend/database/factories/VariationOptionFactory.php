<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Variation;
use Faker\Generator as Faker;
use App\Models\VariationOption;

$factory->define(VariationOption::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'variation_id' =>Variation::select('id')->orderByRaw("RAND()")->first()->id,
    ];
});
