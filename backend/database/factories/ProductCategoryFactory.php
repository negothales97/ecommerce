<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use App\Models\Category;
use Faker\Generator as Faker;
use App\Models\ProductCategory;

$factory->define(ProductCategory::class, function (Faker $faker) {
    return [
        'product_id' => Product::select('id')->orderByRaw("RAND()")->first()->id,
        'category_id' => Category::select('id')->orderByRaw("RAND()")->first()->id,
    ];
});
