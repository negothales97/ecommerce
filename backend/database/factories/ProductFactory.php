<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Color;
use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $promotionalPrice = [
        0.00,
        $faker->randomNumber(2)
    ];
    return [
        'name' => $faker->word,
        'description' => $faker->text,
        'slug' => $faker->unique(true)->slug,
        'meta_title' => $faker->word,
        'meta_description' => $faker->text,
        'measurement_guide' => $faker->text,
        'show' => 1,
        'new' => $faker->boolean($chanceOfGettingTrue = 50),
        'price' => $faker->randomNumber(2),
        'promotional_price' => $promotionalPrice[$faker->unique()->numberBetween($min = 0, $max =1)],
        'stock' => $faker->randomDigit,
        'weight' => 0.5,
        'depth' => 30,
        'width' => 30,
        'height' => 20,
        'sku' => $faker->ean8,
    ];
});
