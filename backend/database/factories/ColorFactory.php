<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Color;
use Faker\Generator as Faker;

$factory->define(Color::class, function (Faker $faker) {
    $files = [
        "color-1.jpg",
        "color-2.jpg",
        "color-3.jpg",
    ];
    return [
        'name' => $faker->colorName,
        'file' => ($files[$faker->numberBetween($min = 0, $max =2)])
    ];
});
