<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use App\Models\Variation;
use Faker\Generator as Faker;
use App\Models\ProductVariation;

$factory->define(ProductVariation::class, function (Faker $faker) {
    return [
        'product_id' => Product::select('id')->orderByRaw("RAND()")->first()->id,
        'variation_id' => Variation::select('id')->orderByRaw("RAND()")->first()->id,
    ];
});
