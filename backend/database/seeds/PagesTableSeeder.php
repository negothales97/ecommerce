<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("pages")->insert([
            [
                "title" => "Quem Somos",
                "slug" => "quem-somos",
                "meta_title" => "Quem Somos",
                "meta_description" => "Quem Somos",
                "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ipsum neque, suscipit id libero ut, dignissim varius nunc. Nam sit amet nulla nec urna interdum egestas ut eu nulla. Curabitur sagittis dapibus ipsum hendrerit tincidunt. Aliquam aliquet diam et pellentesque malesuada. In eu diam id diam interdum sollicitudin. Donec convallis in sem vitae cursus. Nulla et sodales enim. Quisque ullamcorper tortor id lacus semper, quis rhoncus leo elementum. Vestibulum ullamcorper, nibh sed dapibus dictum, nunc urna feugiat felis, nec vestibulum velit eros nec nibh. Donec nec velit hendrerit, eleifend massa vel, placerat orci. Quisque ac enim magna. Sed rutrum leo mauris, nec lacinia sapien tristique quis. Curabitur laoreet laoreet nibh.
Aliquam ut arcu turpis. Nunc sodales erat in luctus imperdiet. Morbi nec congue eros, et condimentum lectus. Proin euismod eleifend urna sit amet maximus. Sed sit amet eros a dolor faucibus tincidunt sed a lectus. Aenean viverra sodales eros. Quisque fringilla sollicitudin sem, nec ullamcorper lorem mollis rutrum. Nullam tempor ipsum massa, in euismod enim lobortis eleifend. Curabitur eget iaculis dui. Nunc nec cursus lacus.",
            ], [
                "title" => "Sobre as Entregas",
                "slug" => "sobre-entregas",
                "meta_title" => "Sobre as Entregas",
                "meta_description" => "Sobre as Entregas",
                "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ipsum neque, suscipit id libero ut, dignissim varius nunc. Nam sit amet nulla nec urna interdum egestas ut eu nulla. Curabitur sagittis dapibus ipsum hendrerit tincidunt. Aliquam aliquet diam et pellentesque malesuada. In eu diam id diam interdum sollicitudin. Donec convallis in sem vitae cursus. Nulla et sodales enim. Quisque ullamcorper tortor id lacus semper, quis rhoncus leo elementum. Vestibulum ullamcorper, nibh sed dapibus dictum, nunc urna feugiat felis, nec vestibulum velit eros nec nibh. Donec nec velit hendrerit, eleifend massa vel, placerat orci. Quisque ac enim magna. Sed rutrum leo mauris, nec lacinia sapien tristique quis. Curabitur laoreet laoreet nibh.
Aliquam ut arcu turpis. Nunc sodales erat in luctus imperdiet. Morbi nec congue eros, et condimentum lectus. Proin euismod eleifend urna sit amet maximus. Sed sit amet eros a dolor faucibus tincidunt sed a lectus. Aenean viverra sodales eros. Quisque fringilla sollicitudin sem, nec ullamcorper lorem mollis rutrum. Nullam tempor ipsum massa, in euismod enim lobortis eleifend. Curabitur eget iaculis dui. Nunc nec cursus lacus.",
            ], [
                "title" => "Formas de Pagamento",
                "slug" => "formas-pagamento",
                "meta_title" => "Formas de Pagamento",
                "meta_description" => "Formas de Pagamento",
                "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ipsum neque, suscipit id libero ut, dignissim varius nunc. Nam sit amet nulla nec urna interdum egestas ut eu nulla. Curabitur sagittis dapibus ipsum hendrerit tincidunt. Aliquam aliquet diam et pellentesque malesuada. In eu diam id diam interdum sollicitudin. Donec convallis in sem vitae cursus. Nulla et sodales enim. Quisque ullamcorper tortor id lacus semper, quis rhoncus leo elementum. Vestibulum ullamcorper, nibh sed dapibus dictum, nunc urna feugiat felis, nec vestibulum velit eros nec nibh. Donec nec velit hendrerit, eleifend massa vel, placerat orci. Quisque ac enim magna. Sed rutrum leo mauris, nec lacinia sapien tristique quis. Curabitur laoreet laoreet nibh.
Aliquam ut arcu turpis. Nunc sodales erat in luctus imperdiet. Morbi nec congue eros, et condimentum lectus. Proin euismod eleifend urna sit amet maximus. Sed sit amet eros a dolor faucibus tincidunt sed a lectus. Aenean viverra sodales eros. Quisque fringilla sollicitudin sem, nec ullamcorper lorem mollis rutrum. Nullam tempor ipsum massa, in euismod enim lobortis eleifend. Curabitur eget iaculis dui. Nunc nec cursus lacus.",
            ], [
                "title" => "Políticas de Privacidade",
                "slug" => "politicas-privacidade",
                "meta_title" => "Políticas de Privacidade",
                "meta_description" => "Políticas de Privacidade",
                "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ipsum neque, suscipit id libero ut, dignissim varius nunc. Nam sit amet nulla nec urna interdum egestas ut eu nulla. Curabitur sagittis dapibus ipsum hendrerit tincidunt. Aliquam aliquet diam et pellentesque malesuada. In eu diam id diam interdum sollicitudin. Donec convallis in sem vitae cursus. Nulla et sodales enim. Quisque ullamcorper tortor id lacus semper, quis rhoncus leo elementum. Vestibulum ullamcorper, nibh sed dapibus dictum, nunc urna feugiat felis, nec vestibulum velit eros nec nibh. Donec nec velit hendrerit, eleifend massa vel, placerat orci. Quisque ac enim magna. Sed rutrum leo mauris, nec lacinia sapien tristique quis. Curabitur laoreet laoreet nibh.
Aliquam ut arcu turpis. Nunc sodales erat in luctus imperdiet. Morbi nec congue eros, et condimentum lectus. Proin euismod eleifend urna sit amet maximus. Sed sit amet eros a dolor faucibus tincidunt sed a lectus. Aenean viverra sodales eros. Quisque fringilla sollicitudin sem, nec ullamcorper lorem mollis rutrum. Nullam tempor ipsum massa, in euismod enim lobortis eleifend. Curabitur eget iaculis dui. Nunc nec cursus lacus.",
            ], [
                "title" => "Políticas de Troca",
                "slug" => "politicas-troca",
                "meta_title" => "Políticas de Troca",
                "meta_description" => "Políticas de Troca",
                "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ipsum neque, suscipit id libero ut, dignissim varius nunc. Nam sit amet nulla nec urna interdum egestas ut eu nulla. Curabitur sagittis dapibus ipsum hendrerit tincidunt. Aliquam aliquet diam et pellentesque malesuada. In eu diam id diam interdum sollicitudin. Donec convallis in sem vitae cursus. Nulla et sodales enim. Quisque ullamcorper tortor id lacus semper, quis rhoncus leo elementum. Vestibulum ullamcorper, nibh sed dapibus dictum, nunc urna feugiat felis, nec vestibulum velit eros nec nibh. Donec nec velit hendrerit, eleifend massa vel, placerat orci. Quisque ac enim magna. Sed rutrum leo mauris, nec lacinia sapien tristique quis. Curabitur laoreet laoreet nibh.
Aliquam ut arcu turpis. Nunc sodales erat in luctus imperdiet. Morbi nec congue eros, et condimentum lectus. Proin euismod eleifend urna sit amet maximus. Sed sit amet eros a dolor faucibus tincidunt sed a lectus. Aenean viverra sodales eros. Quisque fringilla sollicitudin sem, nec ullamcorper lorem mollis rutrum. Nullam tempor ipsum massa, in euismod enim lobortis eleifend. Curabitur eget iaculis dui. Nunc nec cursus lacus.",
            ],
        ]);
    }
}
