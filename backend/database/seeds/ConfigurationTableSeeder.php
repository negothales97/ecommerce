<?php

use Illuminate\Database\Seeder;

class ConfigurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configurations')->insert([
            'cellphone' => "(11) 98822-2222",
            'email' => "sac@keepjeans.com.br",
            'opening_hours' => "Segunda à sexta-feira das 9h às 18hs",
            'about' => "<p>
            A Keep Jeans é uma marca de moda voltada aos públicos feminino e masculino, e tem como aseu
            principal produto desenvolvido o jeans.<br><br>

            Num universo cercado de boas ideias e um público cada vez mais exigente e admirador de novas
            tendências, a Keep visa oferecer as melhores peças de roupas, estudando e pesquisando tudo
            em relação à moda e o que está em alta no mercado.<br><br>

            Assim, seus produtos são desenvolvidos da melhor qualidade, alcançando o principal objetivo
            Keep, que é a total satisfação dos seus consumidores, principalmente dos apreciadores de
            moda.
        </p>",
            "cnpj" => "000000000",
            "zip_code" => "03027-000",
            "street" => "Rua Xavantes",
            "district" => "Brás",
            "number" => "434",
            "city" => "São Paulo",
            "state_id" => 20,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
