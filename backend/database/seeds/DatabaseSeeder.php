<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(ConfigurationTableSeeder::class);
        $this->call(ConfigurationPaymentTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        
        factory(\App\Models\Color::class, 10)->create();
        factory(\App\Models\Product::class, 10)->create();
        factory(\App\Models\Category::class, 10)->create();
        factory(\App\Models\Variation::class, 10)->create();
        factory(\App\Models\VariationOption::class, 100)->create();

        // factory(\App\Models\ProductCategory::class, 300)->create();
        // factory(\App\Models\ProductImage::class, 8)->create();
    }
}
