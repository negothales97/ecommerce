<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('newsletter', 'Api\NewsletterController@store')->name('api.newsletter');
Route::get('product', 'Api\ProductController@index')->name('api.product.index');
Route::get('category', 'Api\CategoryController@index')->name('api.category.index');
Route::get('category/{resourceId}', 'Api\CategoryController@show')->name('api.category.show');
Route::get('variation', 'Api\VariationController@index')->name('api.variation.index');
Route::get('variation/{variation}/', 'Api\VariationController@show')->name('api.variation.show');
Route::get('subproduct/{resource}/', 'Api\SubproductController@show')->name('api.subproduct.show');
Route::delete('subproduct/{resource}/', 'Api\SubproductController@delete')->name('api.subproduct.delete');
Route::get('checkout/consult', 'Api\CheckoutController@consult')->name('api.checkout.consult');
Route::get('checkout/shipping', 'Api\CheckoutController@shipping')->name('api.checkout.shipping');

