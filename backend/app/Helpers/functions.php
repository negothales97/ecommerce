<?php

use App\Models\Category;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

function saveImage($originalImage, $name, $dataPath = null)
{
    if (!$dataPath) {
        $thumbnailPath = public_path() . '/uploads/products/thumbnail/';
        $originalPath = public_path() . '/uploads/products/original/';
    } else {
        $thumbnailPath = $dataPath[0];
        $originalPath = $dataPath[1];
    }

    $fileName = getNameFile($name, $originalImage);

    $image = Image::make($originalImage);
    $height = $image->height() < 750 ? $image->height() : 750;

    $image->resize(null, $height, function ($constraint) {
        $constraint->aspectRatio();
    });
    $image->save($originalPath . $fileName, 100);
    
    $image->resize(250, null, function ($constraint) {
        $constraint->aspectRatio();
    });
    $image->save($thumbnailPath . $fileName, 100);

    return $fileName;
}

function getNameFile($name, $originalImage)
{
    $extension = '.' . File::extension($originalImage->getClientOriginalName());
    $fileName = $name . date('Ymd') . time() . microtime();
    $fileName = str_replace('.', '', $fileName);
    $fileName = str_replace(' ', '', $fileName) . $extension;

    return $fileName;
}

function convertMoneyBrazilToUSA($value)
{
    $value = str_replace(',', '.', str_replace('.', '', $value));
    $value = floatval($value);

    return $value;
}
function convertMoneyUSAToBrazil($value)
{
    $value = number_format($value, 2, ',', '.');

    return $value;
}

function convertDateBrazilToUSA($date)
{
    $date = implode("-", array_reverse(explode("/", $date)));

    return $date;
}

function convertDateUSAToBrazil($date, $format = 'd/m/Y')
{
    // $date = implode("/", array_reverse(explode("-", $date)));
    $date = date($format, strtotime($date));
    return $date;
}

function getSubcategories(Category $category)
{
    if ($category->categories->count() > 0) {
        foreach ($category->categories as $subcategory) {
            getSubcategories($subcategory);
        }
    }
}

function validateRequest(String $input)
{
    if (request()->has($input)) {
        return (request($input) != '');
    }
    return false;
}

function formatData(array $data)
{
    if (isset($data['height'])) {
        $data['height'] = convertMoneyBrazilToUsa($data['height']);
    }
    if (isset($data['width'])) {
        $data['width'] = convertMoneyBrazilToUsa($data['width']);
    }
    if (isset($data['length'])) {
        $data['length'] = convertMoneyBrazilToUsa($data['length']);
    }
    if (isset($data['weight'])) {
        $data['weight'] = convertMoneyBrazilToUsa($data['weight']);
    }
    return $data;
}
function validateResource(array $rules, array $data)
{
    $validator = \Validator::make(
        $data,
        $rules
    );
    return $validator;
}
function clearSpecialCaracteres(String $string)
{
    $specialCaracteres = ['“', '‘', '!', '@', '#', '$', '%', '&', '*', '(', ')', '_', '-', '+', '=', '{', '[', '}', ']', '|', '<', '>', '.', ':', ';', '?', '/'];
    $string = str_replace($specialCaracteres, "", $string);
    return $string;
}

function cmp($a, $b)
{
    // if ($a['product_id'] == $b['product_id']) {
    //     return $a['color_id'] - $b['color_id'];
    // }
    return strcmp($a['product_id'], $b['product_id']);
}

function productSearch($search_string)
{
    if ($search_string == null || $search_string == "") {
        $products = App\Models\Product::get();
    } else {
        $products = App\Models\Product::where('name', 'like', "%{$search_string}%")->get();
    }
    return $products;
}
function convertAmountPagarMe($value)
{
    return number_format($value, 2, '', '');
}

function getTotal($items)
{
    $total = 0;
    foreach ($items as $cart) {
        $product = App\Models\Product::find($cart['product_id']);
        if ($product->promotional_price == 0) {
            $total += $product->price * $cart['quantity'];
        } else {
            $total += $product->promotional_price * $cart['quantity'];
        }
    }
    return $total;
}

function getVariationFilters($products_id)
{
    return \App\Models\Variation
        ::join('product_variations', 'product_variations.variation_id', '=', 'variations.id')
        ->join('products', 'product_variations.product_id', '=', 'products.id')
        ->whereIn('products.id', $products_id)
        ->select('variations.id', 'variations.name')
        ->groupBy('variations.id', 'variations.name')
        ->with('options')
        ->get();
}

function getColorFilters($products_id)
{
    return \App\Models\Color::join('subproducts', 'subproducts.color_id', '=', 'colors.id')
        ->join('products', 'subproducts.product_id', '=', 'products.id')
        ->whereIn('products.id', $products_id)
        ->select('colors.id', 'colors.name', 'colors.file')
        ->groupBy('colors.id', 'colors.name', 'colors.file')
        ->get();

}
