<?php

namespace App\Services;

use App\Models\Product;

class CartService
{
    public static function index()
    {
        $user = auth()->guard('customer')->user();
        $cart = ($user) ? $user->cart->products : session()->get('cart');
        $items = [];
        if ($cart) {
            $i = 0;
            foreach ($cart as $key => $value) {
                $product = Product::find($cart[$key]['product_id']);
                if ($product) {
                    $items[$i] = [
                        'cart_key' => $key,
                        'product' => $product,
                        'product_id' => $cart[$key]['product_id'],
                        'quantity' => $cart[$key]['quantity'],
                    ];
                    $i++;
                } else {
                    unset($cart[$key]);
                }
            }
        }
        if (!$user) {
            usort($cart, "cmp");
            session()->put('cart', $cart);
            session()->save();
        }
        return $items;
    }
}
