<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Subproduct;

class ProductVariationService
{
    public static function create(Product $product, array $data)
    {
        $product->variations()->sync($data['variation_id']);
    }

    public static function delete(Subproduct $subproduct)
    {
        $subproduct->variationOptions()->sync([]);
    }
}