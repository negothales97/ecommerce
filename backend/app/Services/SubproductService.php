<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Subproduct;
use App\Services\ProductVariationService;

class SubproductService
{
    public static function create(array $data, Product $product)
    {
        $subproduct = $product->products()->create([
            'stock' => $product->stock,
            'weight' => $product->weight,
            'depth' => $product->depth,
            'width' => $product->width,
            'height' => $product->height,
            'sku' => $product->sku,
            'barcode' => $product->barcode,
        ]);
        $subproduct->variationOptions()->sync($data['variaton_option_id']);
    }
    public static function update(array $data, Subproduct $subproduct)
    {
        $subproduct->update([
            'stock' => $data['stock'],
            'show' => $data['show'],
            'weight' => \convertMoneyBrazilToUSA($data['weight']),
            'depth' => \convertMoneyBrazilToUSA($data['depth']),
            'width' => \convertMoneyBrazilToUSA($data['width']),
            'height' => \convertMoneyBrazilToUSA($data['height']),
            'sku' => $data['sku'],
            'barcode' => $data['barcode'],
            'color_id' => $data['color_id'],
        ]);
    }

    public static function delete(Subproduct $subproduct)
    {
        ProductVariationService::delete($subproduct);
        $subproduct->delete();
    }

}
