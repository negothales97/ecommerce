<?php

namespace App\Services;

use App\Models\Color;

class ColorService
{
    public static function create(array $data)
    {
        $dataPath = [
            public_path() . '/uploads/colors/thumbnail/',
            public_path() . '/uploads/colors/original/',
        ];
        $data['file'] = saveImage($data['file'], $data['name'], $dataPath);

        return Color::create($data);
    }
    public static function update(array $data, Color $color)
    {
        if (isset($data['file'])) {
            $dataPath = [
                public_path() . '/uploads/colors/thumbnail/',
                public_path() . '/uploads/colors/original/',
            ];
            $data['file'] = saveImage($data['file'], $data['name'], $dataPath);
        }
        $color->fill($data);
        $color->save();
    }

    public static function delete(Color $color)
    {
        $color->delete();
    }
}
