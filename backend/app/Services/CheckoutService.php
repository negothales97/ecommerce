<?php

namespace App\Services;

use PagarMe;
use App\Models\State;
use App\Services\CartService;

class CheckoutService
{

    public static function transactionCreditCard(array $data)
    {
        $customer = auth()->guard('customer')->user();
        $amount = convertAmountPagarMe($data['parcels']);
        $cellphone = clearSpecialCaracteres($data['cellphone']);
        $shipping = \json_decode($data['shipping']);
        $date = date('Y-m-d',strtotime("+{$shipping->delivery_time} days"));
        $items = CartService::index();
        $itemsPagarMe = [];
        foreach($items as $item){
            $itemPagarMe = [
                    'id' => "{$item['product_id']}",
                    'title' => $item['product']->name,
                    'unit_price' => convertAmountPagarMe($item['product']->promotional_price == 0.0 ? $item['product']->price : $item['product']->promotional_price),
                    'quantity' => $item['quantity'],
                    'tangible' => true,
            
            ];
            array_push($itemsPagarMe, $itemPagarMe);
        }

        $pagarme = new PagarMe\Client(config('pagarme.api_key'));
        $transaction = $pagarme->transactions()->create([
            'amount' => convertAmountPagarMe(convertMoneyBrazilToUSA($shipping->new_total)),
            'payment_method' => 'credit_card',
            'card_holder_name' => $data['card_holder_name'],
            'card_cvv' => $data['cvv'],
            'card_number' => clearSpecialCaracteres($data['card_number']),
            'card_expiration_date' => clearSpecialCaracteres($data['validity']),
            'postback_url' => config('pagarme.postback_url'),
            'customer' => [
                'external_id' => "{$customer->id}",
                'name' => $customer->name,
                'type' => $customer->document_type == 'cpf' ? 'individual' : 'corporation',
                'country' => 'br',
                'documents' => [
                    [
                        'type' => $customer->document_type,
                        'number' => $customer->document_number,
                    ],
                ],
                'phone_numbers' => ["+55{$cellphone}"],
                'email' => $customer->email,
            ],
            'billing' => [
                'name' => $customer->name,
                'address' => [
                    'country' => 'br',
                    'street' => $data['street'],
                    'street_number' => $data['number'],
                    'state' => $data['state'],
                    'city' => $data['city'],
                    'neighborhood' => $data['district'],
                    'zipcode' => clearSpecialCaracteres($data['zip_code']),
                ],
            ],
            'shipping' => [
                'name' => $customer->name,
                'fee' => convertAmountPagarMe(convertMoneyBrazilToUSA($shipping->shipping_price)),
                'delivery_date' => $date,
                'expedited' => false,
                'address' => [
                    'country' => 'br',
                    'street' => $data['street'],
                    'street_number' => $data['number'],
                    'state' => $data['state'],
                    'city' => $data['city'],
                    'neighborhood' => $data['district'],
                    'zipcode' => clearSpecialCaracteres($data['zip_code']),
                ],
            ],
            'items' => $itemsPagarMe
        ]);
        return $transaction;
    }

    public static function transactionBoleto(array $data)
    {
        $customer = auth()->guard('customer')->user();
        $amount = convertAmountPagarMe($data['parcels']);
        $cellphone = clearSpecialCaracteres($data['cellphone']);
        $shipping = \json_decode($data['shipping']);
        $date = date('Y-m-d',strtotime("+{$shipping->delivery_time} days"));
        $items = CartService::index();
        $itemsPagarMe = [];
        foreach($items as $item){
            $itemPagarMe = [
                    'id' => "{$item['product_id']}",
                    'title' => $item['product']->name,
                    'unit_price' => convertAmountPagarMe($item['product']->promotional_price == 0.0 ? $item['product']->price : $item['product']->promotional_price),
                    'quantity' => $item['quantity'],
                    'tangible' => true,
            ];
            array_push($itemsPagarMe, $itemPagarMe);
        }


        $pagarme = new PagarMe\Client(config('pagarme.api_key'));
        $transaction = $pagarme->transactions()->create([
            'amount' => convertAmountPagarMe(convertMoneyBrazilToUSA($shipping->new_total)),
            'payment_method' => 'boleto',
            'postback_url' => config('pagarme.postback_url'),
            'customer' => [
                'external_id' => "{$customer->id}",
                'name' => $customer->name,
                'type' => $customer->document_type == 'cpf' ? 'individual' : 'corporation',
                'country' => 'br',
                'documents' => [
                    [
                        'type' => $customer->document_type,
                        'number' => $customer->document_number,
                    ],
                ],
                'phone_numbers' => ["+55{$cellphone}"],
                'email' => $customer->email,
            ],
            'billing' => [
                'name' => $customer->name,
                'address' => [
                    'country' => 'br',
                    'street' => $data['street'],
                    'street_number' => $data['number'],
                    'state' => $data['state'],
                    'city' => $data['city'],
                    'neighborhood' => $data['district'],
                    'zipcode' => clearSpecialCaracteres($data['zip_code']),
                ],
            ],
            'shipping' => [
                'name' => $customer->name,
                'fee' => convertAmountPagarMe(convertMoneyBrazilToUSA($shipping->shipping_price)),
                'delivery_date' => $date,
                'expedited' => false,
                'address' => [
                    'country' => 'br',
                    'street' => $data['street'],
                    'street_number' => $data['number'],
                    'state' => $data['state'],
                    'city' => $data['city'],
                    'neighborhood' => $data['district'],
                    'zipcode' => clearSpecialCaracteres($data['zip_code']),
                ],
            ],
            'items' => $itemsPagarMe
        ]);
        return $transaction;
    }
}
