<?php

namespace App\Services;

use App\Models\Product;

class ProductService
{
    public static function index(array $data, $products_id = null, $per_page = 6)
    {
        $products = new Product();
        if(!strpos(url()->current(),"admin")){
            $products = $products->where('products.show', 1);
        }
        $products = $products->leftjoin('subproducts', 'subproducts.product_id', '=', 'products.id');

        if (\validateRequest('variations')) {
            $variations = \json_decode($data['variations']);
            $products = $products->join('subproduct_variation_options', 'subproduct_variation_options.subproduct_id', '=', 'subproducts.id');
            $products = $products->where(function ($query) use ($variations) {
                foreach ($variations as $variation) {
                    if (count($variation)) {
                        $query = $query->orWhereIn('subproduct_variation_options.variation_option_id', $variation);
                    }
                }
            });
        }
        if (\validateRequest('colors')) {
            $colors = \json_decode($data['colors']);
            $products = $products->whereIn('subproducts.color_id', $colors);
        }
        if ($products_id) {
            $products = $products->whereIn('products.id', $products_id);
        }

        $orderField = validateRequest('order') ? $data['order'] : 'desc';
        if (validateRequest('field')) {
            if ($data['field'] == 'max_price' || $data['field'] == 'min_price') {
                $data['field'] = 'promotional_price = 0, price';
            }
        } else {
            $data['field'] = 'products.updated_at';
        }
        $attributes = [
            'products.id',
            'products.name',
            'products.description',
            'products.slug',
            'products.show',
            'products.primary',
            'products.new',
            'products.sale',
            'products.has_free_shipping',
            'products.promotional_price',
            'products.price',
            'products.updated_at',
        ];
        // dd($products
        //         ->orderByRaw("{$data['field']} $orderField")->get());
        return $products
            ->select($attributes)
            ->orderByRaw("{$data['field']} $orderField")
            ->groupBy($attributes)
            ->paginate($per_page);
    }

    public static function create(array $data)
    {
        return Product::create($data);
    }

    public static function update(array $data, Product $product)
    {
        if (validateRequest('price')) {
            $data['price'] = \convertMoneyBrazilToUSA($data['price']);
        }
        if (validateRequest('promotional_price')) {
            $data['promotional_price'] = \convertMoneyBrazilToUSA($data['promotional_price']);
        }
        if (validateRequest('weight')) {
            $data['weight'] = \convertMoneyBrazilToUSA($data['weight']);
        }
        if (validateRequest('depth')) {
            $data['depth'] = \convertMoneyBrazilToUSA($data['depth']);
        }
        if (validateRequest('width')) {
            $data['width'] = \convertMoneyBrazilToUSA($data['width']);
        }
        if (validateRequest('height')) {
            $data['height'] = \convertMoneyBrazilToUSA($data['height']);
        }
        $product->fill($data);
        $product->save();

        return $product;
    }

    public static function delete(Product $product)
    {
        return $product->delete();
    }
}
