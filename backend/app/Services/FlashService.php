<?php

namespace App\Services;

use App\Models\Flash;
use Illuminate\Http\Request;

class FlashService
{
    public static function index(Request $request)
    {
        return Flash::get();
    }

    public static function create(array $data)
    {
        $data['limit_date'] = \convertDateBrazilToUSA($data['limit_date']);
        $data['discount']= \convertMoneyBrazilToUSA($data['discount']);
        return Flash::create($data);
    }


    public static function update(array $data, Flash $flash)
    {
        
        $data['limit_date'] = \convertDateBrazilToUSA($data['limit_date']);
        $data['discount']= \convertMoneyBrazilToUSA($data['discount']);
        $flash->fill($data);
        $flash->save();

        return $flash;
    }

    public static function delete(Flash $flash)
    {
        return $flash->delete();
    }
}
