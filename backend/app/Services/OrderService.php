<?php

namespace App\Services;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Services\CartService;

class OrderService
{
    public static function create($customer, array $data, $transaction)
    {
        $shipping = \json_decode($data['shipping']);
        $shipping_price= convertMoneyBrazilToUSA($shipping->shipping_price);
        $new_total= convertMoneyBrazilToUSA($shipping->new_total);
        $subtotal = $new_total - $shipping_price;
        $boleto_url = $transaction->payment_method == 'boleto' ? $transaction->boleto_url : null;
        $order = Order::create([
            'customer_id' => $customer->id,
            'shipping_type' => $shipping->type,
            'shipping_price' => $shipping_price,
            'delivery_time' => $shipping->delivery_time,
            'subtotal' => $subtotal,
            'boleto_url' => $boleto_url,
            'total' => $new_total,
            'payment_status' => $transaction->status,
            'transaction_id' => $transaction->tid,
        ]);
        $items = CartService::index();
        foreach($items as $item)
        {
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item['product_id'],
                'quantity' => $item['quantity'],
                'unit_price' => $item['product']->promotional_price == 0.00 ?$item['product']->price  : $item['product']->promotional_price,
            ]);
        }
        return $order;
        
    }
}