<?php

namespace App\Services;

class ShippingService
{
    public static function getShippingPrice(array $data)
    {
        $data = http_build_query($data);
        $url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx";
        // cURL
        $curl = curl_init($url . '?' . $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);
        //!end cUrl
        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $json = json_decode($json, true);
        if (isset($json['cServico']['Erro']) && $json['cServico']['Erro'] != 0) {
            throw new \Exception('Problema em buscar os dados');
        }
        return $json;
    }

    public static function generateArrayShipping($zip_code, $shipping_type)
    {
        // $weight = ShippingService::getWeight();
        // $volumeTotal = ShippingService::getVolume();
        $cepDestino = clearSpecialCaracteres(config('correios.cep_destino'));
        // $data['nCdEmpresa'] = config('correios.empresa');
        // $data['sDsSenha'] = config('correios.senha');
        $data['sCepOrigem'] = clearSpecialCaracteres($zip_code);
        $data['sCepDestino'] = $cepDestino;
        $data['nCdServico'] = $shipping_type;

        $data['nVlPeso'] = 2;
        $data['nCdFormato'] = '1';

        $data['nVlComprimento'] = 35;
        $data['nVlAltura'] = 32;
        $data['nVlLargura'] = 33;
        $data['nVlDiametro'] = '0';
        $data['sCdMaoPropria'] = 'n';
        $data['nVlValorDeclarado'] = 0;
        $data['sCdAvisoRecebimento'] = 'n';

        $data['StrRetorno'] = 'xml';
        return $data;
    }
    public static function getWeight()
    {
        $carts = session()->get('cart');
        $weight = 0;
        foreach ($carts as $cart) {
            $product = Product::find($cart['product_id']);
            $weight += 0.5 * $cart['quantity'];
        }
        $weight = number_format($weight, 0, '', '');

        return $weight;
    }

    public static function getVolume()
    {
        $carts = session()->get('cart');

        $volumeTotal = 0;
        foreach ($carts as $item) {
            $volume = $item->volume * $item->pivot->qty;
            $volumeTotal += $volume;
        }
        return $volumeTotal;
    }
    
}
