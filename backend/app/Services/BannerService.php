<?php

namespace App\Services;

use App\Models\Banner;

class BannerService
{
    public function status(Banner $banner)
    {
        if ($banner->status == 1) {
            Banner::where('id', '<>', $banner->id)
                ->update(['status' => '0']);
        }
    }
}
