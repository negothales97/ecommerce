<?php

namespace App\Observers;

use App\Models\Banner;
use App\Services\BannerService;

class BannerObserver
{
    protected $bannerService;
    public function __construct()
    {
        $this->bannerService = new BannerService();
    }
    public function created(Banner $banner)
    {
        $this->bannerService->status($banner);
    }

    /**
     * Handle the banner "updated" event.
     *
     * @param  \App\Banner  $banner
     * @return void
     */
    public function updated(Banner $banner)
    {
        $this->bannerService->status($banner);
    }

    /**
     * Handle the banner "deleted" event.
     *
     * @param  \App\Banner  $banner
     * @return void
     */
    public function deleted(Banner $banner)
    {
        //
    }

    /**
     * Handle the banner "restored" event.
     *
     * @param  \App\Banner  $banner
     * @return void
     */
    public function restored(Banner $banner)
    {
        //
    }

    /**
     * Handle the banner "force deleted" event.
     *
     * @param  \App\Banner  $banner
     * @return void
     */
    public function forceDeleted(Banner $banner)
    {
        //
    }
}
