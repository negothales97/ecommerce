<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'slug',
        'meta_title',
        'meta_description',
        'show',
        'tag_id',
        'primary',
        'new',
        'sale',
        'has_free_shipping',
        'promotional_price',
        'price',
        'stock',
        'measurement_guide',
        'weight',
        'depth',
        'use_subproduct',
        'width',
        'height',
        'sku',
        'barcode',
    ];

    protected $appends = [
        'main_image',
        'toggle',
    ];

    protected $dates = [
        'updated_at',
    ];

    public function images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Subproduct', 'product_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'product_categories', 'product_id', 'category_id');
    }

    public function variations()
    {
        return $this->belongsToMany('App\Models\Variation', 'product_variations', 'product_id', 'variation_id');
    }
    public function tag()
    {
        return $this->belongsTo('App\Models\Tag');
    }

    public function mainProductImage()
    {
        return $this->images()->where('position', 1)->first();
    }

    public function getMainImageAttribute($value)
    {
        if (count($this->images)) {
            return asset('uploads/products/thumbnail/' . $this->images->firstWhere('position', 1)->file);
        } else {
            return asset('img/no-photo-50.png');
        }
    }
    public function getToggleAttribute($value)
    {
        return $this->attributes['show'] == 1 ? 'toggle-on' : 'toggle-off';
    }
    public function getUpdatedAtAttribute($value)
    {
        return convertDateUSAToBrazil($this->attributes['updated_at']);
    }

}
