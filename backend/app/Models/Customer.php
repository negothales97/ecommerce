<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $guard = 'customer';

    protected $fillable = [
        'name',
        'email',
        'document_type',
        'document_number',
        'birthday',
        'cellphone',
        'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cart()
    {
        return $this->hasOne('App\Models\Cart', 'email', 'email');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\CustomerAddress', 'customer_id');
    }
    public function cards()
    {
        return $this->hasMany('App\Models\CustomerCard', 'customer_id');
    }
    public function getBirthdayAttribute($value)
    {
        return \convertDateUSAToBrazil($value);
    }
    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = convertDateBrazilToUsa($value);
    }

}
