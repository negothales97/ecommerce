<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $fillable= [
        'cellphone',
        'email',
        "opening_hours",
        "about",
        "cnpj",
        "zip_code",
        "street",
        "district",
        "number",
        "city",
        "state_id",
        "instagram",
        "facebook",
        "youtube",
        "linkedin",
        "tiktok",
        "twitter",
        "pinterest",
    ];

    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_id');
    }
}
