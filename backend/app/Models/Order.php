<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'customer_id',
        'shipping_type',
        'shipping_price',
        'delivery_time',
        'subtotal',
        'boleto_url',
        'total',
        'status',
        'payment_status',
        'tracking_code',
        'transaction_id',
    ];

    public function getStatusAttribute($value)
    {
        $status = [
            0 => 'Pedido gerado',
            1 => 'Nota Fiscal emitida',
            2 => 'Pedido enviado',
        ];
        return $status[$value];
    }

    public function getPaymentStatusAttribute($value)
    {
        $status = [
            'processing' => 'Processo de autorização',
            'authorized' => 'Transação autorizada',
            'paid' => 'Pago',
            'refunded' => 'Transação Estornada',
            'waiting_payment' => 'Aguardando Pagamento',
            'refused' => 'Transação Recusada',
        ];
        return $status[$value];
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\OrderProduct', 'order_id');
    }
}
