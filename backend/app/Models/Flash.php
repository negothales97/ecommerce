<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flash extends Model
{
    protected $fillable = [
        'limit_date',
        'limit_hour',
        'discount',
        'name',
    ];
}
