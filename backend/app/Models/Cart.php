<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
        'email',
    ];

    public function products()
    {
        return $this->hasMany('App\Models\CartProduct', 'cart_id');
    }
}
