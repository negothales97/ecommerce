<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'name',
        'file',
        'link',
        'target',
        'status',
    ];

    public function getStatusAttribute($value)
    {
        $status = [
            '0' => "Inativo",
            '1' => "Ativo"
        ];
        return $status[$value] ?? 1;

    }
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] =$value;
    }
}
