<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parent_id' => $this->parent_id,
            'slug' => $this->slug,
            'meta_description' => $this->meta_description,
            'meta_title' => $this->meta_title,
            'meta_description' => $this->meta_description,
            'featured' => $this->featured,
            'status' => $this->status,
            'url'=> route('category',['slug' => $this->slug]),
            'categories'=> $this->categories
        ];
    }
}
