<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $search_string = $request->search_string;

        $products = productSearch($search_string);
        $products = $products->filter(function ($product) {
            if ($product->images()->count() > 0) {
                return $product;
            }
        });
        $products_id = $products->pluck('id')->toArray();
        $variationFilters = getVariationFilters($products_id);
        $colorFilters = getColorFilters($products_id);

        $products = ProductService::index($request->all(), $products_id);
        return view('customer.pages.search.index')
            ->with('variationFilters', $variationFilters)
            ->with('colorFilters', $colorFilters)
            ->with('products', $products);
    }
}
