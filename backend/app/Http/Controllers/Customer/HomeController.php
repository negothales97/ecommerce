<?php

namespace App\Http\Controllers\Customer;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $promotions = Product::where('show', 1)
        ->where('promotional_price', '<>', 0.00)
        ->limit(20)
        ->get();

        $novelties = Product::where('new', 1)
        ->limit(20)->get();
        
        $fastPromotions = Product::get();
        $featuredCategories=  Category::where('featured', 1)->get();
        return view('customer.pages.home.index')
            ->with('fastPromotions', $fastPromotions)
            ->with('promotions', $promotions)
            ->with('novelties', $novelties)
            ->with('featuredCategories', $featuredCategories);
    }
}
