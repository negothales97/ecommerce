<?php

namespace App\Http\Controllers\Customer;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Services\CartService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    public function index()
    {
        $items = CartService::index();
        $total = getTotal($items);

        return view('customer.pages.cart.index')
            ->with('total', $total)
            ->with('items', $items);
    }
    public function store(Request $request)
    {
        $product = Product::find($request->product_id);
        $user = Auth::guard('customer')->user();
        $cart = session()->get('cart');
        if (!$cart) {
            $cart = [];
            $i = 0;
        } else {
            $keys = array_keys($cart);
            $i = end($keys);
            $i++;
        }
        $cart[$i] = [
            'product_id' => $request->product_id,
            'color' => $request->color,
            // 'variations' => $variations,
            'quantity' => $request->qty,
        ];
        usort($cart, "cmp");
        if ($user) {
            if (!$user->cart) {
                Cart::create(['email' => $user->email]);
            }
            $user->cart->products()->create($cart[$i]);
        }else{
            session()->put('cart', $cart);
            session()->save();
        }
        // $variations = [];
        // foreach ($product->variations as $variation) {
        //     \array_push($variations, $request['variation-' . $variation->id]);
        // }
        
        return redirect()->route('cart.index');
    }

    public function update(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        if ($customer) {

        }
        $cart = session()->get('cart');
        $key = array_key_first($request->quantity);
        $cart[$key]['quantity'] = $request->quantity[$key];

        usort($cart, "cmp");
        session()->put('cart', $cart);
        session()->save();

        return redirect()->back()
            ->with('success', 'Item atualizado');
    }
    public function delete(Request $request)
    {
        $customer = Auth::guard('customer')->user();
        if ($customer) {

        }
        $cart = session()->get('cart');
        if (isset($cart[$request->id])) {
            unset($cart[$request->id]);
        }
        usort($cart, "cmp");
        session()->put('cart', $cart);
        session()->save();

        return redirect()->back()
            ->with('success', 'Item removido');
    }
}
