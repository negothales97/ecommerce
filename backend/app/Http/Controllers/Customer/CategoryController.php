<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Services\ProductService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request, $slug)
    {
        $category = Category::where('slug', $slug)->first();

        if ($this->findCategory($category)) {
            return \redirect('/');
        }
        $products = $category->products;

        
        $products = $products->filter(function ($product) {
            if ($product->images()->count() > 0) {
                return $product;
            }
        });
        $products_id = $products->pluck('id')->toArray();
        $variationFilters = getVariationFilters($products_id);
        $colorFilters = getColorFilters($products_id);
        
        $products = ProductService::index($request->all(), $products_id);
        return view('customer.pages.category.index')
            ->with('products', $products)
            ->with('variationFilters', $variationFilters)
            ->with('colorFilters', $colorFilters)
            ->with('category', $category);
    }

    public function findCategory($category)
    {
        if (!$category) {
            return true;
        }
        if ($category->status == 0) {
            return true;
        }
        return false;
    }
}
