<?php

namespace App\Http\Controllers\Customer;

use App\Models\Color;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index($slug)
    {
        $product = Product::where('slug', $slug)->first();

        $subproductId = $product->products()->pluck('id')->toArray();

        $variationQuery = \DB::table('variation_options')
            ->join('variations', 'variations.id', '=', 'variation_options.variation_id')
            ->join('subproduct_variation_options', 'subproduct_variation_options.variation_option_id', '=', 'variation_options.id')
            ->whereIn('subproduct_id', $subproductId);
        $variations = $variationQuery
            ->select(
                'variations.id',
                'variations.name',
            )
            ->groupBy('variations.id', 'variations.name')
            ->get();

        $variationOptions = $variationQuery
            ->select(
                'variation_options.id',
                'variation_options.variation_id',
                'variation_options.name',
            )
            ->groupBy('variation_options.id', 'variation_options.name', 'variation_options.variation_id')
            ->get();

        $categoriesId = $product->categories()
            ->pluck('categories.id')
            ->toArray();

        $colors = Color::whereIn('id', $product->products()->pluck('color_id')->toArray())->get();

        $relatedProducts = $categoriesId != [] ? Product::join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->where('category_id', $categoriesId)
            ->where('products.id', "<>", $product->id)->get() : collect([]);

        return view('customer.pages.product.index')
            ->with('variations', $variations)
            ->with('colors', $colors)
            ->with('variationOptions', $variationOptions)
            ->with('relatedProducts', $relatedProducts)
            ->with('product', $product);
    }
}
