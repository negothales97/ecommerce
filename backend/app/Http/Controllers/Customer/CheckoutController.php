<?php

namespace App\Http\Controllers\Customer;

use Auth;
use App\Models\State;
use App\Models\Product;
use App\Models\Customer;
use App\Services\CartService;
use App\Services\OrderService;
use App\Services\CheckoutService;
use App\Notifications\CreatedOrder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CheckoutRequest;

class CheckoutController extends Controller
{
    public function index()
    {
        $items = CartService::index();
        $total = getTotal($items);
        $states = State::get();
        return view('customer.pages.checkout.index')
            ->with('items', $items)
            ->with('states', $states)
            ->with('total', $total);
    }
    public function success()
    {
        return view('customer.pages.checkout.success');
    }

    public function email()
    {
        $customer = auth()->guard('customer')->user();
        if ($customer) {
            return redirect()
                ->route('cart.checkout.index');
        }
        return view('customer.pages.checkout.email');
    }

    public function store(CheckoutRequest $request)
    {
        $data = $request->all();
        $customer = Auth::guard('customer')->user();
        if (!$customer) {
            $data['birthday'] = \convertDateBrazilToUSA($data['birthday']);
            $data['password'] = Hash::make($data['password']);
            $data['state_id'] = State::where('sigla', $data['state'])->first()->id;
            $customer = Customer::create($data);
            $customer->addresses()->create($data);
            if($request->payment_method == 'credit_card'){
                $customer->cards()->create($data);
            }
            Auth::guard('customer')->loginUsingId($customer->id);
        }else{
            $customer->fill($data);
            $customer->save();
        }
        $transaction = null;
        if ($data['payment_method'] == 'credit_card') {
            $transaction = CheckoutService::transactionCreditCard($data);
        }else if($data['payment_method']== 'boleto'){
            $transaction = CheckoutService::transactionBoleto($data);
        }  

        $order = OrderService::create($customer, $data, $transaction);
        // session()->forget('cart');
        // $customer->cart->products->delete();
        // $customer->cart->delete();
        $customer->notify(new CreatedOrder($order));
        \Session::flash('order_id', $order->id);
        return redirect()->route('cart.checkout.success');

    }
}
