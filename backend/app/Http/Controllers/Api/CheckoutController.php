<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Services\ShippingService;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
    public function consult(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);
        $customer = Customer::where('email', $request->email)->first();
        if ($customer) {
            return \response()->json($customer);
        }
        return response()->json("Nao Localizado", 404);
    }

    public function shipping(Request $request)
    {
        $dataPac = ShippingService::generateArrayShipping($request->cep, "04510");
        $dataSedex = ShippingService::generateArrayShipping($request->cep, "04014");

        $shippingPac = ShippingService::getShippingPrice($dataPac);
        $shippingSedex = ShippingService::getShippingPrice($dataSedex);
        // try {
            $newTotalPac = $request->total + convertMoneyBrazilToUsa($shippingPac['cServico']['Valor']);
            $newTotalSedex = $request->total + convertMoneyBrazilToUsa($shippingSedex['cServico']['Valor']);
            $array = [
                [
                    'type' => 'PAC',
                    'delivery_time' => $shippingPac['cServico']['PrazoEntrega'],
                    'shipping_price' => ($shippingPac['cServico']['ValorSemAdicionais']),
                    'new_total' => convertMoneyUSAtoBrazil($newTotalPac),
                ],
                [
                    'type' => 'SEDEX',
                    'delivery_time' => $shippingSedex['cServico']['PrazoEntrega'],
                    'shipping_price' => ($shippingSedex['cServico']['ValorSemAdicionais']),
                    'new_total' => convertMoneyUSAtoBrazil($newTotalSedex),
                ],
            ];
            return response()->json(
                $array
            );
        // } catch (\Exception $e) {
        //     return 
        //     response()->json(
        //         'error: ' . $e->getMessage(), 500
        //     );
        // }
    }

}
