<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if(\validateRequest('category_id')){
            $category = Category::find($request->category_id);
            $products = $category->products;
        }else{
            $products = productSearch($request->search_string);
        }
        $products = $products->filter(function ($product) {
            if ($product->images()->count() > 0) {
                return $product;
            }
        });

        $products_id = $products->pluck('id')->toArray();
        $products = ProductService::index($request->all(), $products_id);
        return view('customer.components.filter-products')
        ->with('products', $products);
    }
}
