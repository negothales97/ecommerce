<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\CategoryService;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\Category as CategoryResource;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->model = Category::class;
        $this->modelRequest = new CategoryRequest();
    }
    public function index(Request $request)
    {
        $resources = $this->model::with('categories')->paginate($request->per_page);

        return \response()->json($resources,200);
    }

    public function show($resourceId, Request $request)
    {
        
        $resource = new CategoryResource($this->model::with('categories')->findOrFail($resourceId));
        return \response()->json($resource,200);

    }
}
