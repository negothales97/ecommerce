<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\BaseController;

class PageController extends BaseController
{
    public function __construct()
    {
        $this->model = Page::class;
        $this->path = 'page';
        $this->modelRequest = new PageRequest();
    }
}
