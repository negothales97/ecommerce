<?php

namespace App\Http\Controllers\Admin;

use App\Models\Flash;
use Illuminate\Http\Request;
use App\Services\FlashService;
use App\Http\Requests\FlashRequest;
use App\Http\Controllers\Controller;

class FlashController extends Controller
{
    public function index(Request $request)
    {
        $flashs = FlashService::index($request);
        return view('admin.pages.flash.index')
            ->with('flashs', $flashs);
    }
    public function create()
    {
        return view('admin.pages.flash.create');
    }

    public function store(FlashRequest $request)
    {
        $flash = FlashService::create($request->all());
        return \redirect()
            ->route('admin.promotion.flash.edit', ['flash' => $flash])
            ->with('success', 'Promoção relâmpago cadastrada com sucesso');
    }
    public function edit(Flash $flash)
    {
        return view('admin.pages.flash.edit')
            ->with('flash', $flash);
    }

    public function update(Flash $flash, FlashRequest $request)
    {
        FlashService::update($request->all(), $flash);
        return \redirect()
            ->back()
            ->with('success', 'Promoção editada com sucesso');
    }

    public function delete(Flash $flash)
    {
        FlashService::delete($flash);
        return \redirect()
            ->back()
            ->with('success', 'Promoção removida com sucesso');
    }
}
