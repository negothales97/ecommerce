<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{

    public function index()
    {
        $orders = Order::get();
        return view('admin.pages.order.index')
            ->with('orders', $orders);
    }

    public function edit(Order $order)
    {
        return view('admin.pages.order.edit')
            ->with('order', $order);
    }

    public function update(OrderRequest $request, Order $order)
    {
        $order->update([
            "zip_code" => $request["zip_code"],
            "street" => $request["street"],
            "number" => $request["number"],
            "district" => $request["district"],
            "city" => $request["city"],
            "state" => $request["state"],
            "status" => $request["status"],
            "tracking_code" => $request["tracking_code"],
        ]);
        return redirect()
        ->back()
        ->with('success', 'Pedido atualizado com sucesso');
    }
}
