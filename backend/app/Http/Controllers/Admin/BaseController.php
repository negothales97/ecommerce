<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    protected $path;
    protected $dataPath;
    protected $model;
    protected $modelRequest;

    public function index(Request $request)
    {
        $resources = $this->model::paginate(20);
        return view("admin.pages.{$this->path}.index")
            ->with('resources', $resources);
    }

    public function create()
    {
        return view("admin.pages.{$this->path}.create");
    }

    public function store(Request $request, $resourceId = null)
    {
        
        $validator = validateResource(
            $this->modelRequest->rules(),
            $request->all()
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->all();
        if (isset($data['file'])) {
            $data['file'] = saveImage($data['file'], $data['name'], $this->dataPath);
        }
        $data = formatData($data);

        $this->model::create($data);

        return redirect()->route("admin.{$this->path}.index")
            ->with('success', 'Dados adicionados com sucesso');
    }

    public function edit($resourceId)
    {
        $resource = $this->model::find($resourceId);
        return view("admin.pages.{$this->path}.edit")
            ->with('resource', $resource);
    }

    public function update(Request $request, $resourceId)
    {
        $validator = validateResource(
            $this->modelRequest->rules(),
            $request->all()
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $id = validateRequest('resourceId') ? $request->resourceId : $resourceId;
        $data = $request->all();

        if (isset($data['file'])) {
            $data['file'] = saveImage($data['file'], $data['name'], $this->dataPath);
        }
        $data = formatData($data);

        $resource = $this->model::find($id);
        $resource->fill($data);
        try {
            $resource->save();
            return redirect()->back()
                ->with('success', 'Dados atualizados com sucesso');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()
                ->with('error', 'Tivemos um problema no servidor, entre em contato com um administrador');
        }

    }
    public function status($resourceId)
    {
        $resource = $this->model::find($resourceId);

        $resource->status = $resource->status == 1 ? 0 : 1;
        $resource->save();

        return redirect()->back()
            ->with('success', 'Status atualizado com sucesso');
    }

    public function delete($resourceId)
    {
        $resource = $this->model::find($resourceId);

        $resource->delete();
        return redirect()->back()
            ->with('success', 'Dados removidos com sucesso');
    }
}
