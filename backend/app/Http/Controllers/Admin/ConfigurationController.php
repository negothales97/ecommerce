<?php

namespace App\Http\Controllers\Admin;

use App\Models\State;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Http\Controllers\Controller;

class ConfigurationController extends Controller
{
    protected $configuration;

    public function __construct()
    {
        $this->configuration = Configuration::first();
    }

    public function edit()
    {
        $states = State::get();
        return view('admin.pages.configuration.edit')
        ->with('states', $states)
        ->with('configuration', $this->configuration);
    }
    public function update(Request $request)
    {
        
        $data = $request->all();

        $this->configuration->fill($request->all());
        $this->configuration->save();
        return redirect()->back()->with('success', 'Dados atualizados com sucesso ');
    }
}
