<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannerRequest;
use App\Http\Controllers\Admin\BaseController;

class BannerController extends BaseController
{
    public function __construct()
    {
        $this->model = Banner::class;
        $this->path = 'design.banner';
        $this->modelRequest = new BannerRequest();
        $this->dataPath = [
            public_path() . '/uploads/design/banner/thumbnail/',
            public_path() . '/uploads/design/banner/original/',
        ];
    }


    public function change($resourceId)
    {
        $resource = $this->model::findOrfail($resourceId);
        if ($resource->status == 0) {
            $this->model::where('status', 1)->update(['status' => 0]);
            $resource->update(['status' => 1]);
        }else{
            $resource->update(['status' => 0]);
        }
        return redirect()->route("admin.{$this->path}.index")
            ->with('success', 'Dados atualizados com sucesso');
    }
}
