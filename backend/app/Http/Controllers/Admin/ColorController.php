<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ColorRequest;
use App\Models\Color;
use App\Services\ColorService;

class ColorController extends Controller
{
    public function index()
    {
        $colors = Color::get();
        return view('admin.pages.color.index')
            ->with('colors', $colors);
    }

    public function create()
    {
        return view('admin.pages.color.create');
    }
    public function store(ColorRequest $request)
    {

        $color = ColorService::create($request->all());

        return redirect()->route('admin.color.edit', ['color' => $color])
            ->with('success', 'Cor adicionada com sucesso');
    }

    public function edit(Color $color)
    {
        return view('admin.pages.color.edit')
            ->with('color', $color);
    }
    public function update(ColorRequest $request, Color $color)
    {
        ColorService::update($request->all(), $color);

        return redirect()->back()
            ->with('success', 'Cor atualizada com sucesso');
    }

    public function delete(Color $color)
    {
        ColorService::delete($color);
        return redirect()->back()
            ->with('success', 'Cor removida com sucesso');
    }
}
