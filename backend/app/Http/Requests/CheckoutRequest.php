<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $rules = [
        //     'name' => 'required',
        //     'zip_code' => 'required',
        //     'shipping' => 'required',
        //     'street' => 'required',
        //     'number' => 'required',
        //     'district' => 'required',
        //     'document_number' => 'required',
        //     'state' => 'required',
        //     'city' => 'required',
        //     'payment_method' => 'required',
        // ];
        // if(auth()->guard('customer')->check()){
        // }else{
        //     // $rules['email']= 'required|unique:customers|email';
        //     // $rules['password']= 'required|min:6|confirmed';
        //     $rules['birthday']= 'required';
        //     $rules['cellphone']= 'required';
        //     $rules['document_number']= 'required';
        // }
        
        // return $rules;
        return [];
    }
}
