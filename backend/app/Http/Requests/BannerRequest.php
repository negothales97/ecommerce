<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = last(request()->segments());
        $file = ($id == 'store') ? 'required|mimes:png,jpg,jpeg' : 'nullable|mimes:png,jpg,jpeg';
        return [
            'name' => 'required',
            'file' => $file
        ];
    }
}
