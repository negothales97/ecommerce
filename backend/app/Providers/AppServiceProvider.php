<?php

namespace App\Providers;

use DB;
use App\Models\Banner;
use App\Models\Category;
use App\Observers\BannerObserver;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Facades\Blade;
use Faker\Generator as FakerGenerator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FakerGenerator::class, function () {
            return FakerFactory::create('pt_BR');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $paymentConfiguration = DB::table('configuration_payments')->first();
        $mainCategories = \App\Models\Category::whereNull('parent_id')->where('featured', 1)->get();
        $categories = \App\Models\Category::whereNull('parent_id')->get();
        $configuration = DB::table('configurations')
            ->join('states', 'configurations.state_id', '=', 'states.id')
            ->select('states.sigla', 'configurations.*')
            ->first();

        Banner::observe(BannerObserver::class);

        Blade::component('components.input', 'input');

        \View::share('paymentConfiguration', $paymentConfiguration);
        \View::share('configuration', $configuration);
        \View::share('mainCategories', $mainCategories);
        \View::share('categories', $categories);
    }
}
