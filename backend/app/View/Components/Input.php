<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    protected $name;
    protected $clazz;
    protected $label;
    protected $value;
    protected $attribute;

    public function __construct($name, $clazz, $label, $value, $attribute)
    {
        $this->name = $name;
        $this->clazz = $clazz;
        $this->value = $value;
        $this->attribute = $attribute;
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }
}
